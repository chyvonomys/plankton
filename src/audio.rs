#[cfg(feature = "audio")]
use rodio::{self, Source};
#[cfg(feature = "audio")]
use std;
#[cfg(feature = "audio")]
use time;

#[cfg(feature = "audio")]
type Src = rodio::Source<Item = f32> + Send;

#[cfg(feature = "audio")]
struct Track {
    sink: rodio::Sink,
    buff_id: usize,
    sus_round: usize,
}

#[cfg(feature = "audio")]
use std::collections::HashMap;
#[cfg(feature = "audio")]
use std::collections::hash_map::Entry;

#[cfg(feature = "audio")]
use screen::{Entity, EntityId};

#[derive(Debug, Serialize, Deserialize)]
pub struct SoundId(usize); // creation is private to this mod

#[derive(Debug, Serialize, Deserialize)]
pub struct AudioItem {
    pub id: SoundId,
    pub volume: f32,
}

impl SoundId {
    pub fn d3note1sec() -> Self {
        SoundId(0)
    }

    pub fn title_music() -> Self {
        SoundId(1)
    }

    pub fn level_music() -> Self {
        SoundId(2)
    }
}

#[cfg(feature = "audio")]
pub struct AudioSystem {
    endpoint: rodio::Device,
    buffers: Vec<rodio::source::Buffered<rodio::buffer::SamplesBuffer<f32>>>,
    tracks: HashMap<EntityId, Track>,
    current_sus_round: usize,
}

#[cfg(feature = "audio")]
impl AudioSystem {
    pub fn new() -> Self {
        let endpoint = rodio::default_output_device().unwrap();

        //let title_file = std::fs::File::open("./resources/donor.ogg").unwrap();
        //let title_music_source = rodio::Decoder::new(std::io::BufReader::new(title_file)).unwrap();

        //let level_file = std::fs::File::open("./resources/omen.ogg").unwrap();
        //let level_music_source = rodio::Decoder::new(std::io::BufReader::new(level_file)).unwrap();

        let one_sec = std::time::Duration::from_secs(1);

        use rodio::source::SineWave;
        let d3note: Box<Src> = Box::new(SineWave::new(147).take_duration(one_sec));
        let csh3note: Box<Src> = Box::new(SineWave::new(138).take_duration(one_sec));
        let b2note: Box<Src> = Box::new(SineWave::new(123).take_duration(one_sec));

        //let title_music: Box<Src> = Box::new(title_music_source.convert_samples::<f32>());
        //let level_music: Box<Src> = Box::new(level_music_source.convert_samples::<f32>());

        let mut sources = vec![d3note, csh3note, b2note];
        sources.reverse();
        let mut buffered_iters = Vec::new();

        while let Some(it) = sources.pop() {
            let before = time::PreciseTime::now();
            let res = rodio::buffer::SamplesBuffer::new(
                it.channels(),
                it.sample_rate(),
                it.collect::<Vec<_>>(),
            ).buffered();

            let mid = time::PreciseTime::now();
            let nsamples = res.clone().count();
            let after = time::PreciseTime::now();
            println!(
                "warmup nsamples: {}, took: {}/{}",
                nsamples,
                before.to(mid),
                mid.to(after)
            );
            buffered_iters.push(res);
        }

        Self {
            endpoint: endpoint,
            buffers: buffered_iters,
            tracks: HashMap::new(),
            current_sus_round: 0,
        }
    }

    fn sustain(&mut self, eid: EntityId, id: usize, vol: f32) {
        let entry = self.tracks.entry(eid);

        match entry {
            Entry::Occupied(mut o) => {
                let vi = o.get_mut();
                if vi.buff_id != id {
                    vi.sink.stop();
                    vi.sink.append(self.buffers[id].clone());
                    vi.buff_id = id;
                }
                vi.sink.set_volume(vol);
                vi.sus_round = self.current_sus_round;
            }
            Entry::Vacant(v) => {
                let mut sink = rodio::Sink::new(&self.endpoint);
                sink.append(self.buffers[id].clone());
                sink.set_volume(vol);
                v.insert(Track {
                    sink: sink,
                    buff_id: id,
                    sus_round: self.current_sus_round,
                });
            }
        }
    }

    fn maintain(&mut self) {
        // drop all tracks that haven't been sustained
        for v in self.tracks.values() {
            if v.sus_round < self.current_sus_round {
                v.sink.stop();
            }
        }
        self.current_sus_round += 1;
    }

    pub fn turn(&mut self, es: &::screen::Entities) {
        for (id, e) in es.all() {
            if let Entity {
                active: true,
                audio:
                    Some(AudioItem {
                        id: SoundId(sid),
                        volume,
                    }),
                ..
            } = *e
            {
                self.sustain(id, sid, volume);
            }
        }

        self.maintain();
    }
}
