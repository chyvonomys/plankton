use glium;
use glium::glutin::VirtualKeyCode as VKC;
use glium::glutin::{ElementState, Event, KeyboardInput, WindowEvent};
use math::Vec2;
use math::consts::*;

#[derive(PartialEq)]
pub enum Action { Press, Release }

#[derive(Default)]
pub struct InputInfo {
    pub left: Vec<(Action, f64)>,
    pub right: Vec<(Action, f64)>,
    pub up: Vec<(Action, f64)>,
    pub down: Vec<(Action, f64)>,
    pub action: Vec<(Action, f64)>,
    pub jump: Vec<(Action, f64)>,
    pub save: Vec<(Action, f64)>,
    pub load: Vec<(Action, f64)>,
    pub shutdown: bool,
    pub mouse_pos: Vec2,
}

pub struct Snapshot {
    pub left: bool,
    pub right: bool,
    pub up: bool,
    pub down: bool,
    pub action: bool,
    pub jump: bool,
}

impl InputInfo {
    pub fn get_snapshot(&self) -> Snapshot {
        Snapshot {
            left: self.left.last().map_or(false, |p| p.0 == Action::Press),
            right: self.right.last().map_or(false, |p| p.0 == Action::Press),
            up: self.up.last().map_or(false, |p| p.0 == Action::Press),
            down: self.down.last().map_or(false, |p| p.0 == Action::Press),
            action: self.action.last().map_or(false, |p| p.0 == Action::Press),
            jump: self.jump.last().map_or(false, |p| p.0 == Action::Press),
        }
    }
}

pub struct InputSystem {
    pub events: InputInfo,
    window_size: Vec2,
    scale: f32,
}

impl InputSystem {
    pub fn new(w: u32, h: u32, scale: f32) -> Self {
        Self {
            events: InputInfo::default(),
            window_size: Vec2::new(w as f32, h as f32),
            scale: scale,
        }
    }

    pub fn turn(&mut self, now: f64, evloop: &mut glium::glutin::EventsLoop) {
        evloop.poll_events(|ev| {
            // println!("ev: {:?}", ev);
            if let Event::WindowEvent { event: ev, .. } = ev {
                match ev {
                    WindowEvent::CloseRequested |
                    WindowEvent::KeyboardInput {
                        input: KeyboardInput {
                            state: ElementState::Pressed,
                            virtual_keycode: Some(VKC::Escape),
                            ..
                        },
                        ..
                    } => self.events.shutdown = true,
                    WindowEvent::KeyboardInput {
                        input: KeyboardInput {
                            state: st,
                            virtual_keycode: Some(vk),
                            ..
                        },
                        ..
                    } => {
                        // NOTE: matching both variants lets do following trick:
                        //       press A, press Left, release A, ... left action still has effect.
                        if let Some(list) = match vk {
                            VKC::Left  | VKC::A      => Some(&mut self.events.left),
                            VKC::Right | VKC::D      => Some(&mut self.events.right),
                            VKC::Up    | VKC::W      => Some(&mut self.events.up),
                            VKC::Down  | VKC::S      => Some(&mut self.events.down),
                            VKC::K                   => Some(&mut self.events.action),
                            VKC::J                   => Some(&mut self.events.jump),
                            VKC::F2                  => Some(&mut self.events.save),
                            VKC::F3                  => Some(&mut self.events.load),
                            _ => None,
                        } {
                            let a = match st {
                                ElementState::Pressed => Action::Press,
                                ElementState::Released => Action::Release,
                            };
                            // Do not register 'repetition' events.
                            if list.last().map(|i| i.0 == a) != Some(true) {
                                list.push((a, now));
                            }
                        };
                    }
                    WindowEvent::Resized(s) => self.window_size = Vec2::new(s.width as f32, s.height as f32),
                    WindowEvent::CursorMoved {
                        position, ..
                    } => self.events.mouse_pos = Vec2::new(
                        self.scale * position.x as f32,
                        self.window_size.y - ONE - self.scale * position.y as f32,
                    ),
                    _ => (),
                }
            }
        });
    }
}
