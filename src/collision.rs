use screen::{Entities, Entity, EntityId, PositionItem};
use math::{self, Vec2};
use math::consts::*;

#[derive(Clone, PartialEq, Serialize, Deserialize, Debug)]
pub enum CollisionResponse {
    Free,
    PushesLeft(EntityId),
    PushesRight(EntityId),
    StandsOn(EntityId),
    StandsOnAndPushesLeft(EntityId, EntityId),
    StandsOnAndPushesRight(EntityId, EntityId),
    Supports(EntityId),
}

#[derive(Debug, PartialEq)]
enum CollisionResponseInternal {
    Free,
    PushesLeft(usize),
    PushesRight(usize),
    StandsOn(usize),
    StandsOnAndPushesLeft(usize, usize),
    StandsOnAndPushesRight(usize, usize),
    Supports(usize),
}

impl CollisionResponseInternal {
    fn combine_with(&mut self, incoming: CollisionResponseInternal) {
        use self::CollisionResponseInternal::*;
        *self = match (&*self, incoming) {
            (&PushesLeft(e1), StandsOn(e2)) => StandsOnAndPushesLeft(e2, e1),
            (&PushesRight(e1), StandsOn(e2)) => StandsOnAndPushesRight(e2, e1),
            (&StandsOn(e1), PushesLeft(e2)) => StandsOnAndPushesLeft(e1, e2),
            (&StandsOn(e1), PushesRight(e2)) => StandsOnAndPushesRight(e1, e2),
            (_, x) => x,
        }
    }

    fn to_external(&self, is: &[Item]) -> CollisionResponse {
        use self::CollisionResponseInternal::*;
        match *self {
            Free => CollisionResponse::Free,
            StandsOn(j) => CollisionResponse::StandsOn(is[j].id),
            Supports(j) => CollisionResponse::Supports(is[j].id),
            PushesLeft(j) => CollisionResponse::PushesLeft(is[j].id),
            PushesRight(j) => CollisionResponse::PushesRight(is[j].id),
            StandsOnAndPushesLeft(j, k) => CollisionResponse::StandsOnAndPushesLeft(is[j].id, is[k].id),
            StandsOnAndPushesRight(j, k) => CollisionResponse::StandsOnAndPushesRight(is[j].id, is[k].id),
        }
    }
}

impl CollisionResponse {
    pub fn get_pushes(&self) -> Option<EntityId> {
        match *self {
            CollisionResponse::PushesLeft(ref e) => Some(*e),
            CollisionResponse::PushesRight(ref e) => Some(*e),
            CollisionResponse::StandsOnAndPushesLeft(_, ref e) => Some(*e),
            CollisionResponse::StandsOnAndPushesRight(_, ref e) => Some(*e),
            _ => None
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum CollisionShape {
    Rect(f32, f32),
}

// TODO: this item only makes sense if PositionItem is present
// problem: item dependencies. maybe this should be a member of positionitem?
// problem2: dependencies on multiple items?
#[derive(Debug, Serialize, Deserialize)]
pub struct MovementItem {
    pub planned_position: Vec2,
}

impl MovementItem {
    pub fn new(p: &Vec2) -> Self {
        Self {
            planned_position: p.clone(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CollisionItem {
    pub shape: CollisionShape,
    response: CollisionResponse,
}

impl CollisionItem {
    pub fn rect(w: f32, h: f32) -> Self {
        Self {
            shape: CollisionShape::Rect(w, h),
            response: CollisionResponse::Free,
        }
    }

    #[inline]
    pub fn get_response(&self) -> &CollisionResponse {
        &self.response
    }
}

#[derive(Debug)]
struct MovingShape {
    shape: CollisionShape,
    curr: Vec2,
    next: Vec2,
    finish: Vec2,
}

impl MovingShape {
    fn still(&self) -> Self {
        Self {
            shape: self.shape.clone(),
            curr: self.curr.clone(),
            next: self.curr.clone(),
            finish: self.curr.clone(),
        }
    }
}

#[derive(Debug)]
struct Item {
    id: EntityId,
    m: MovingShape,
    hit: CollisionResponseInternal,
}

#[derive(Debug, PartialEq)]
enum OverlapRange {
    None,
    To(f32),
    From(f32),
    Range(f32, f32),
    Everywhere,
}

fn check_01_overlap(r: (f32, f32), eps: f32) -> OverlapRange {
    // Build OverlapRange -- range of t during which two shapes overlap
    // This range is basically an intersection of (0, 1) and [t0, t1]

    let zero = ZERO + eps;
    let one = ONE - eps;

    if r.0 < zero {
        if r.1 < zero {
            OverlapRange::None
        } else if r.1 > one {
            OverlapRange::Everywhere
        } else {
            OverlapRange::To(r.1)
        }
    } else if r.0 > one {
        // t1 >= t0
        OverlapRange::None
    } else if r.1 > one {
        OverlapRange::From(r.0)
    } else {
        OverlapRange::Range(r.0, r.1)
    }
}

#[test]
fn test_check_01_overlap() {
    let eps = 0.0001f32;
    let func = |a, b| check_01_overlap((a, b), eps);

    assert_eq!(func(-5.0f32, -2.0f32), OverlapRange::None); // completely before
    assert_eq!(func(2.0f32, 5.0f32), OverlapRange::None); // completeley after
    assert_eq!(func(-5.0f32, 0.0f32), OverlapRange::None); // touches before
    assert_eq!(func(1.0f32, 5.0f32), OverlapRange::None); // touches after
    assert_eq!(func(0.0f32, 0.0f32), OverlapRange::None); // 0.0
    assert_eq!(func(1.0f32, 1.0f32), OverlapRange::None); // 1.0

    assert_eq!(func(0.0f32, 1.0f32), OverlapRange::Everywhere); // same
    assert_eq!(func(-1.0f32, 1.0f32), OverlapRange::Everywhere); // same + before
    assert_eq!(func(-1.0f32, 2.0f32), OverlapRange::Everywhere); // cover all
    assert_eq!(func(0.0f32, 2.0f32), OverlapRange::Everywhere); // same + after

    assert_eq!(func(0.5f32, 0.6f32), OverlapRange::Range(0.5f32, 0.6f32)); // inner range
    assert_eq!(func(0.5f32, 0.5f32), OverlapRange::Range(0.5f32, 0.5f32)); // inner point
    assert_eq!(func(0.5f32, 1.0f32), OverlapRange::From(0.5f32)); // inner touch 1
    assert_eq!(func(0.0f32, 0.5f32), OverlapRange::To(0.5f32)); // inner touch 0

    assert_eq!(func(-1.0f32, 0.5f32), OverlapRange::To(0.5f32)); // before .. inner
    assert_eq!(func(0.5f32, 2.0f32), OverlapRange::From(0.5f32)); // inner .. after
}

const EVERYWHERE: (f32, f32) = (::std::f32::MIN, ::std::f32::MAX);

fn solve_abs_ineq(a: f32, b: f32, m: f32) -> Option<(f32, f32)> {
    if a == 0f32 {
        if b.abs() < m { Some(EVERYWHERE) } else { None }
    } else {
        let x0 = (-b - m) / a;
        let x1 = (-b + m) / a;
        Some((x0.min(x1), x0.max(x1)))
    }
}

#[test]
fn test_solve_abs_ineq() {
    assert_eq!(solve_abs_ineq(ZERO, ZERO, ONE), Some(EVERYWHERE));
    assert_eq!(solve_abs_ineq(ZERO, ONE, ZERO), None);
    assert_eq!(solve_abs_ineq(ZERO, ONE, ONE), None); // < not <=

    assert_eq!(solve_abs_ineq(ONE, ZERO, ONE), Some((-ONE, ONE)));
    assert_eq!(solve_abs_ineq(-ONE, ZERO, ONE), Some((-ONE, ONE)));

    assert_eq!(solve_abs_ineq(ONE, -ONE, ONE), Some((ZERO, TWO)));
    assert_eq!(solve_abs_ineq(-ONE, ONE, ONE), Some((ZERO, TWO)));

    assert_eq!(solve_abs_ineq(ONE, ONE, ONE), Some((-TWO, ZERO)));
    assert_eq!(solve_abs_ineq(-ONE, -ONE, ONE), Some((-TWO, ZERO)));

    assert_eq!(solve_abs_ineq(TWO, -ONE, ONE), Some((ZERO, ONE)));
    assert_eq!(solve_abs_ineq(0.5f32, -ONE, ONE), Some((ZERO, FOUR)));
}

fn range_intersection(r0: Option<(f32, f32)>, r1: Option<(f32, f32)>) -> Option<(f32, f32)> {
    match (r0, r1) {
        (Some((r0from, r0to)), Some((r1from, r1to))) => {
            let from = r0from.max(r1from);
            let to = r0to.min(r1to);
            if from < to { Some((from, to)) } else { None }
        }
        _ => None
    }
}

#[test]
fn test_range_intersection() {
    let zero_one = Some((ZERO, ONE));
    let one_three = Some((ONE, 3f32));
    let two_four = Some((TWO, FOUR));
    let two_three = Some((TWO, 3f32));

    assert_eq!(range_intersection(None, None), None);
    assert_eq!(range_intersection(zero_one, None), None);
    assert_eq!(range_intersection(None, zero_one), None);
    assert_eq!(range_intersection(zero_one, zero_one), zero_one);

    assert_eq!(range_intersection(one_three, two_four), two_three); // intersect 
    assert_eq!(range_intersection(two_four, one_three), two_three); // intersect rev

    assert_eq!(range_intersection(zero_one, one_three), None); // touch
    assert_eq!(range_intersection(one_three, zero_one), None); // touch rev

    assert_eq!(range_intersection(zero_one, two_three), None); // non overlap
    assert_eq!(range_intersection(two_three, zero_one), None); // non overlap rev
}

// If there is such t [0..1] that /A-B/ = /(wa+wb, ha+hb)/ then there is touch
// Take minimum t of those and that is moment of touch.
// Range of t where /A-B/ < /(wa+wb, ha+hb)/ is where two rects overlap

// /|A.x - B.x|, |A.y - B.y|/ < /(wa + wb, ha + hb)/
// /|a.x0 + a.dx * t - b.x0 - b.dx * t|, |a.y0 + a.dy * t - b.y0 - b.dy * t|/ <= ...
// /|a.x0 - b.x0 + (a.dx - b.dx) * t|, |a.y0 - b.y0 + (a.dy - b.dy) * t| <= ...

// dx0 = a.x0 - b.x0
// dy0 = a.y0 - b.y0
// ddx = a.dx - b.dx
// ddy = a.dy - b.dy

// /|dx0 + ddx * t|, |dy0 + ddy * t|/ <= /(wa + wb, ha + hb)/
// |ax + b| < m  -->  between (-b - m)/a .. (-b + m)/a

// |dx0 + ddx * t| < wa + wb
// |dy0 + ddy * t| < ha + hb

// find these ranges for both, and find intersection

fn collide_rr(
    dd: &Vec2, d0: &Vec2,
    wa: f32, ha: f32, wb: f32, hb: f32, bias: f32,
) -> Option<(f32, f32)> {
    let r1 = solve_abs_ineq(dd.x, d0.x, wa + wb + bias);
    let r2 = solve_abs_ineq(dd.y, d0.y, ha + hb + bias);
    range_intersection(r1, r2)
}

#[test]
fn test_collide_rr() {
    let func = |x0, y0, x1, y1| collide_rr(
        &math::sub(&Vec2::new(x1, y1), &Vec2::new(x0, y0)),
        &Vec2::new(x0, y0),
        ONE, ONE, TWO, ONE, ZERO,
    );
    assert_eq!(func(ZERO, -FOUR, ZERO, FOUR), Some((0.25f32, 0.75f32)));
    assert_eq!(func(ZERO, -TWO, ZERO, TWO), Some((ZERO, ONE)));
    assert_eq!(func(-ONE, -FOUR, ONE, -FOUR), None);
    assert_eq!(func(-ONE, -TWO, ONE, -TWO), None);
    assert_eq!(func(-TWO, FOUR, TWO, FOUR), None);
    assert_eq!(func(-TWO, TWO, TWO, TWO), None);
    // TODO: more tests               
}

fn get_touch_t(range: (f32, f32)) -> Option<Option<f32>> {
    match check_01_overlap(range, 1e-5f32) {
        OverlapRange::None => None,
        OverlapRange::Everywhere => Some(None),
        OverlapRange::To(_x) => Some(None),
        OverlapRange::From(x) => Some(Some(x)),
        OverlapRange::Range(x0, _x1) => Some(Some(x0)),
    }
}

fn collide(a: &MovingShape, b: &MovingShape, bias: f32) -> Option<Option<f32>> {
    // A = a.curr + (a.finish - a.curr) * t;
    // B = b.curr + (b.finish - b.curr) * t;

    let dd = math::sub(&math::sub(&a.finish, &a.curr), &math::sub(&b.finish, &b.curr));
    let d0 = math::sub(&a.curr, &b.curr);

    match (&a.shape, &b.shape) {
        (&CollisionShape::Rect(wa, ha), &CollisionShape::Rect(wb, hb)) =>
            collide_rr(&dd, &d0, wa, ha, wb, hb, bias),
    }.and_then(|r| get_touch_t(r))
}


#[test]
fn test_collide() {
    let sta = MovingShape {
        curr: Vec2::new(ZERO, ZERO),
        next: Vec2::new(ZERO, ZERO),
        finish: Vec2::new(ZERO, ZERO),
        shape: CollisionShape::Rect(ONE, ONE),
    };

    let behind = Vec2::new(3f32, ZERO);

    let test_pair = |a: &MovingShape, b: &MovingShape| {
        if let Some(Some(t)) = collide(a, b, 0.001f32) {
            let anext = math::lerp(&a.curr, &a.finish, t);
            let bnext = math::lerp(&b.curr, &b.finish, t);
            let ovr = match (&a.shape, &b.shape) {
                (&CollisionShape::Rect(wa, ha), &CollisionShape::Rect(wb, hb)) => {
                    (anext.x - bnext.x).abs() < wa + wb && (anext.y - bnext.y).abs() < ha + hb
                },
            };
            assert!(!ovr);
        }
        else {
            panic!()
        }
    };

    for i in 0..101 {
        let t = i as f32 / 100f32;
        let pt = math::lerp(&Vec2::new(-3f32, -8f32), &Vec2::new(-3f32, 8f32), t);

        let mov1 = MovingShape {
            curr: behind.clone(),
            next: behind.clone(),
            finish: pt.clone(),
            shape: CollisionShape::Rect(ONE, ONE),
        };

        test_pair(&sta, &mov1);
        test_pair(&mov1, &sta);

        let mov2 = MovingShape {
            curr: pt.clone(),
            next: pt.clone(),
            finish: behind.clone(),
            shape: CollisionShape::Rect(ONE, ONE),
        };

        test_pair(&sta, &mov2);
        test_pair(&mov2, &sta);
    }
}

// NOTE: correct only for touching shapes or results of horizontal slide
fn check_stand(
    this_contact_pos: &Vec2, that_contact_pos: &Vec2,
    this_shape: &CollisionShape, that_shape: &CollisionShape) -> bool {

    match (this_shape, that_shape) {
        (&CollisionShape::Rect(this_w, _this_h), &CollisionShape::Rect(that_w, _that_h)) => {
            let above = this_contact_pos.y > that_contact_pos.y;
            let within = (this_contact_pos.x - that_contact_pos.x).abs() < this_w + that_w;
            above && within
        },
        // _ => false,
    }
}

#[test]
fn test_check_stand() {
    let player = Vec2::new(234.0007f32, 250f32);
    let platform = Vec2::new(300f32, 187.5f32);
    let player_shape = CollisionShape::Rect(16f32, 50f32);
    let platform_shape = CollisionShape::Rect(50f32, 12.5f32);
    assert_eq!(check_stand(&player, &platform, &player_shape, &platform_shape), true);
}

// NOTE: correct only for touching shapes or results of vertical slide
fn check_push(
    this_contact_pos: &Vec2, that_contact_pos: &Vec2,
    this_shape: &CollisionShape, that_shape: &CollisionShape) -> bool {

    match (this_shape, that_shape) {
        (&CollisionShape::Rect(_this_w, this_h), &CollisionShape::Rect(_that_w, that_h)) => {
            let isleft = this_contact_pos.x < that_contact_pos.x;
            let within = (this_contact_pos.y - that_contact_pos.y).abs() < this_h + that_h;
            isleft && within
        },
        // _ => false,
    }
}

// not only return fact of conflict but also return a magnitude of conflict
fn conflicting_movement0(ad: &Vec2, bd: &Vec2, ab: &Vec2) -> Option<f32> {
    let ad_proj_ab = math::dot(&ad, &ab); // these are projections times ab.len()
    let bd_proj_ab = math::dot(&bd, &ab); // but its not necessary for comparison

    if bd_proj_ab < ad_proj_ab { Some(bd_proj_ab - ad_proj_ab) } else { None }
}

#[cfg(test)]
fn conflicting_movement(a0: &Vec2, a1: &Vec2, b0: &Vec2, b1: &Vec2) -> bool {
    let ad = &math::sub(a1, a0);
    let bd = &math::sub(b1, b0);
    let ab = &math::sub(b0, a0);

    conflicting_movement0(ad, bd, ab).is_some()
}

#[test]
fn test_conflicting_movement() {
    let a0 = Vec2::new(ZERO, ONE);
    let b0 = Vec2::new(ZERO, -ONE);

    let xs = [-10f32, -3f32, 0f32, 7f32, 11f32];

    // a fixed b fixed
    assert_eq!(conflicting_movement(&a0, &a0, &b0, &b0), false);

    // b fixed, a moves horizontally
    for xa in &xs { assert_eq!(conflicting_movement(&a0, &Vec2::new(*xa, ONE), &b0, &b0), false); }

    // b fixed, a moves horizontally and down
    for xa in &xs { assert_eq!(conflicting_movement(&a0, &Vec2::new(*xa, ZERO), &b0, &b0), true); }

    // a fixed, b moves horizontally
    for xb in &xs { assert_eq!(conflicting_movement(&a0, &a0, &b0, &Vec2::new(*xb, -ONE)), false); }

    // a fixed, b moves horizontally and up
    for xb in &xs { assert_eq!(conflicting_movement(&a0, &a0, &b0, &Vec2::new(*xb, ZERO)), true); }

    // b moves horizontally, a moves horizontally
    for xa in &xs {
        for xb in &xs {
            assert_eq!(conflicting_movement(&a0, &Vec2::new(*xa, ONE), &b0, &Vec2::new(*xb, -ONE)), false);
        }
    }

    // b moves horizontally and down, a moves horizontally and up
    for xa in &xs {
        for xb in &xs {
            assert_eq!(conflicting_movement(&a0, &Vec2::new(*xa, TWO), &b0, &Vec2::new(*xb, -TWO)), false);
        }
    }

    // b moves horizontally and up, a moves horizontally and down
    for xa in &xs {
        for xb in &xs {
            assert_eq!(conflicting_movement(&a0, &Vec2::new(*xa, ZERO), &b0, &Vec2::new(*xb, ZERO)), true);
        }
    }
}

// TODO: literal corner case testing needed
fn conflicting_shape_movement(a: &MovingShape, b: &MovingShape) -> Option<f32> {
    calc_direction(&a.curr, &b.curr, &a.shape, &b.shape).and_then(
        |ab| conflicting_movement0(
            &math::sub(&a.finish, &a.curr),
            &math::sub(&b.finish, &b.curr),
            &ab,
        )
    )
}

// returns normal vector of touch from a towards b
fn calc_direction(a: &Vec2, b: &Vec2, ashape: &CollisionShape, bshape: &CollisionShape) -> Option<Vec2> {
    const UP: Vec2 = Vec2 {x: ZERO, y: ONE};
    const DOWN: Vec2 = Vec2 {x: ZERO, y: -ONE};
    const LEFT: Vec2 = Vec2 {x: -ONE, y: ZERO};
    const RIGHT: Vec2 = Vec2 {x: ONE, y: ZERO};

    match (ashape, bshape) {
        (&CollisionShape::Rect(aw, ah), &CollisionShape::Rect(bw, bh)) => {
            let within_vert = (a.y - b.y).abs() < ah + bh;
            let within_horz = (a.x - b.x).abs() < aw + bw;

                 if a.x > b.x && within_vert { Some(LEFT) }
            else if a.x < b.x && within_vert { Some(RIGHT) }
            else if a.y > b.y && within_horz { Some(DOWN) }
            else if a.y < b.y && within_horz { Some(UP) }
            else { None }
        },
    }
}

// NOTE: analysis assumes that they touch
fn recalc_course(
    this_contact_pos: &Vec2, final_point: &Vec2, that_contact_pos: &Vec2,
    this_shape: &CollisionShape, that_shape: &CollisionShape) -> Vec2 {

    let nc = calc_direction(this_contact_pos, that_contact_pos, this_shape, that_shape);

    if let Some(nc) = nc {
        let rem = math::sub(final_point, this_contact_pos);
        let perp = math::perp(&nc);
        let proj = math::dot(&rem, &perp);
        let slide = math::mul(proj, &perp);
        math::add(this_contact_pos, &slide)
    } else {
        this_contact_pos.clone()
    }
}

#[test]
fn test_recalc_course() {
    let player = Vec2::new(234.0007f32, 250f32);
    let platform = Vec2::new(300f32, 187.5f32);
    let player_shape = CollisionShape::Rect(16f32, 50f32);
    let platform_shape = CollisionShape::Rect(50f32, 12.5f32);

    let player_dst = math::add(&player, &Vec2::new(ZERO, -100f32));

    let calc = recalc_course(&player, &player_dst, &platform, &player_shape, &platform_shape);

    println!("calc: {:?}, should be: {:?}", calc, player);

    assert_eq!(math::len(&math::sub(&calc, &player)) < 0.00001f32, true);
}

#[test]
fn test_recalc_course2() {
    let fall = Vec2::new(7f32, 7f32);
    let fly = Vec2::new(9f32, 8f32);
    let shape = CollisionShape::Rect(ONE, ONE);

    let fall_dst = Vec2::new(7f32, 5f32);
    let fly_dst = Vec2::new(5f32, 8f32);

    let same_vec = |a: &Vec2, b: &Vec2| { math::len(&math::sub(a, b)) < 0.00001f32 };

    {
        let calc_fall = recalc_course(&fall, &fall_dst, &fly, &shape, &shape);
        let corr_fall = &fall_dst;
        println!("calc_fall: {:?}, should be: {:?}", &calc_fall, corr_fall);
        assert_eq!(same_vec(&calc_fall, corr_fall), true);
    }

    {
        let calc_fly = recalc_course(&fly, &fly_dst, &fall, &shape, &shape);
        let corr_fly = &fly;
        println!("calc_fly: {:?}, should be: {:?}", &calc_fly, corr_fly);
        assert_eq!(same_vec(&calc_fly, corr_fly), true);
    }
}

pub struct CollisionSystem {
    items: Vec<Item>,
}

impl CollisionSystem {
    pub fn new() -> Self {
        Self {
            items: Vec::new(),
        }
    }

    pub fn turn(&mut self, es: &mut Entities) {
        self.items.clear();

        // Take in account active entities with collision item
        for (id, e) in es.all_mut() {
            if let Entity {
                active: true,
                position: Some(PositionItem { ref origin, .. }),
                ref movement,
                collision: Some(CollisionItem { ref shape, .. }),
                ..
            } = *e
            {
                self.items.push(Item {
                    id,
                    m: MovingShape {
                        shape: shape.clone(),
                        curr: origin.clone(),
                        next: origin.clone(),
                        finish: match *movement {
                            Some(MovementItem { ref planned_position }) => planned_position,
                            None => origin,
                        }.clone(),
                    },
                    hit: CollisionResponseInternal::Free,
                });
            }
        }
        
        let mut is = 0;

        let mut resolve_log = String::new();

        let limit = self.items.len() * 3;

        let bias = 0.01f32; // units

        loop {
            let done = resolve_step(&mut self.items, bias, &mut resolve_log).is_none();

            if done { break } else { is += 1; }

            if is == limit {
                println!("iteration limit");
            }
        }

        for i in &self.items {
            if let Some(&mut Entity {
                movement: Some(MovementItem { ref mut planned_position, .. }),
                collision: Some(CollisionItem { ref mut response, .. }),
                ..
            }) = es.get_mut(i.id)
            {
                *planned_position = i.m.curr.clone();
                *response = i.hit.to_external(&self.items);
            }
        }
    }
}

// It's important that check_overlap use THE SAME as collide,
// That's why we reuse it here
fn check_overlap(a: &MovingShape, b: &MovingShape) -> bool {
    let a1 = a.still();
    let b1 = b.still();

    collide(&a1, &b1, ZERO).is_some()
}

#[test]
fn test_lerp() {
    let t = 0.0010882966f32;
    let a = Vec2::new(350f32, 37.5f32);
    let b = Vec2::new(350f32, 37.5f32);
    println!("lerp: {:?} --{}--> {:?}   is   {:?}", a, t, b, math::lerp(&a, &b, t));

    let aaa = 350.0;
    let bbb = 350.0;
    let xxx = (1.0 - t) * aaa + t * bbb;
    println!("wild: {}", xxx); // --> 349.99997

    let yyy = aaa + (bbb - aaa) * t;
    println!("wild: {}", yyy); // --> 350
}

fn calc_pair_resp(xs: &[Item], i: usize, j: usize) -> (CollisionResponseInternal, CollisionResponseInternal) {
    use self::CollisionResponseInternal::*;

    let mi = &xs[i].m;
    let mj = &xs[j].m;
    
         if check_stand(&mi.next, &mj.next, &mi.shape, &mj.shape) { (StandsOn(j), Supports(i)) }
    else if check_stand(&mj.next, &mi.next, &mj.shape, &mi.shape) { (Supports(j), StandsOn(i)) }
    else if check_push( &mi.next, &mj.next, &mi.shape, &mj.shape) { (PushesRight(j), PushesLeft(i)) }
    else if check_push( &mj.next, &mi.next, &mj.shape, &mi.shape) { (PushesLeft(j), PushesRight(i)) }
    else { (Free, Free) }
}

fn update_min_tuple(
    current: &mut Option<(usize, usize, f32)>,
    candidate: (usize, usize, f32),
    _debug: &mut String) {
    let t = candidate.2;
    if let Some((ref mut mini, ref mut minj, ref mut mint)) = *current {
        #[cfg(test)] _debug.push_str(&format!("{}  -->  {}, ", *mint, t));
        if t < *mint {
            *mini = candidate.0;
            *minj = candidate.1;
            *mint = t;
            if t < ZERO { #[cfg(test)] _debug.push_str("*************************"); }
            #[cfg(test)] _debug.push_str("[change]");
        } else { #[cfg(test)] _debug.push_str("[keep]"); }
    } else {
        *current = Some(candidate);
        #[cfg(test)] _debug.push_str(&format!("--> {} [first]", t));
    }
}

fn resolve_step(xs: &mut [Item], bias: f32, _debug: &mut String) -> Option<(usize, usize, f32)> {
    // Find what pair collide first (if any)
    let mut mintuple = None;
    for i in 0..xs.len() {
        for j in i+1..xs.len() {
            #[cfg(test)] _debug.push_str(&format!("{} and {}: ", i, j));
            // Do not take overlapping pairs into account
            if !check_overlap(&xs[i].m, &xs[j].m) {
                match collide(&xs[i].m, &xs[j].m, bias) {
                    None => { #[cfg(test)] _debug.push_str("don't collide"); }
                    Some(None) => {
                        #[cfg(test)] _debug.push_str("soft overlap");
                        if let Some(p) = conflicting_shape_movement(&xs[i].m, &xs[j].m) {
                            #[cfg(test)] _debug.push_str(", conflicting ");
                            update_min_tuple(&mut mintuple, (i, j, p), _debug);
                        } else {
                            #[cfg(test)] _debug.push_str(", non-conflicting");
                        }
                    }
                    Some(Some(t)) => {
                        #[cfg(test)] _debug.push_str("collide ");
                        update_min_tuple(&mut mintuple, (i, j, t), _debug);
                    }
                }
            } else { #[cfg(test)] _debug.push_str("hard overlap"); }
            #[cfg(test)] _debug.push_str("\n");
        }
    }

    if let Some((i, j, in_t)) = mintuple {
        // negative in_t means t is zero but we are resolving conflicting movements here
        let t = in_t.max(ZERO);
        #[cfg(test)] _debug.push_str(&format!("min tuple is ({} {} {}({}))\n", i, j, in_t, t));
        #[cfg(test)] _debug.push_str(&format!("i: {:?}\n", xs[i]));
        #[cfg(test)] _debug.push_str(&format!("j: {:?}\n", xs[j]));

        #[cfg(test)] _debug.push_str("===================\n");
        for x in xs.iter_mut() {
            x.m.next = math::lerp(&x.m.curr, &x.m.finish, t);
        }
        #[cfg(test)]
        for (i, x) in xs.iter_mut().enumerate() {
            _debug.push_str(&format!("{}: {:?} = lerp({:?} {:?} {})\n", i, x.m.next, x.m.curr, x.m.finish, t));
        }
        #[cfg(test)] _debug.push_str("===================\n");

        let adj_finish = {
            let mi = &xs[i].m;
            let mj = &xs[j].m;
            let fi = recalc_course(&mi.next, &mi.finish, &mj.next, &mi.shape, &mj.shape);
            let fj = recalc_course(&mj.next, &mj.finish, &mi.next, &mj.shape, &mi.shape);
            #[cfg(test)] _debug.push_str(&format!("adj {} curr: {:?} --> {:?}   finish: {:?} --> {:?}\n", i, mi.curr, mi.next, mi.finish, fi));
            #[cfg(test)] _debug.push_str(&format!("adj {} curr: {:?} --> {:?}   finish: {:?} --> {:?}\n", j, mj.curr, mj.next, mj.finish, fj));
            (fi, fj)
        };
        xs[i].m.finish = adj_finish.0;
        xs[j].m.finish = adj_finish.1;

        let (respi, respj) = calc_pair_resp(&xs, i, j);
        #[cfg(test)] _debug.push_str(&format!("(at {}), {}: {:?} += {:?}  {}: {:?} += {:?}\n", t, i, xs[i].hit, respi, j, xs[j].hit, respj));
        xs[i].hit.combine_with(respi);
        xs[j].hit.combine_with(respj);
        #[cfg(test)] _debug.push_str(&format!("now {}: {:?}  now {}: {:?}\n", i, xs[i].hit, j, xs[j].hit));
    } else {
        for x in xs.iter_mut() {
            x.m.next = x.m.finish.clone();
        }
        #[cfg(test)] _debug.push_str(&format!("advance all to finish\n"));
    }

    // Check if responses still are valid
    for i in 0..xs.len() {
        use self::CollisionResponseInternal::*;
        let adj_resp = match xs[i].hit {
            Free => Free,
            StandsOn(j) | Supports(j) | PushesLeft(j) | PushesRight(j) => {
                calc_pair_resp(&xs, i, j).0
            }
            StandsOnAndPushesLeft(j, k) | StandsOnAndPushesRight(j, k) => {
                let mut pj = calc_pair_resp(&xs, i, j);
                let pk = calc_pair_resp(&xs, i, k);
                pj.0.combine_with(pk.0);
                pj.0
            }
        };

        let adjust = if adj_resp == Free {
            true
        } else if let StandsOnAndPushesLeft(j, k) = xs[i].hit {
            match adj_resp {
                StandsOn(jj) if jj == j => true,
                PushesLeft(kk) if kk == k => true,
                _ => false,
            }
        } else if let StandsOnAndPushesRight(j, k) = xs[i].hit {
            match adj_resp {
                StandsOn(jj) if jj == j => true,
                PushesRight(kk) if kk == k => true,
                _ => false,
            }
        } else { false };

        if adjust {
            xs[i].hit = adj_resp;
        }
    }

    for x in xs.iter_mut() {
        x.m.curr = x.m.next.clone();
    }

    mintuple
}

#[cfg(test)]
fn make_item(i: usize, cs: &CollisionShape, p0: &Vec2, p1: &Vec2) -> Item {
    Item {
        id: ::screen::make_test_eid(i),
        m: MovingShape {
            shape: cs.clone(),
            curr: p0.clone(),
            next: p0.clone(),
            finish: p1.clone(),
        },
        hit: CollisionResponseInternal::Free,
    }
}

#[cfg(test)]
fn make_unit_item(i: usize, x0: f32, y0: f32, x1: f32, y1: f32) -> Item {
    Item {
        id: ::screen::make_test_eid(i),
        m: MovingShape {
            shape: CollisionShape::Rect(ONE, ONE),
            curr: Vec2::new(x0, y0),
            next: Vec2::new(x0, y0),
            finish: Vec2::new(x1, y1),
        },
        hit: CollisionResponseInternal::Free,
    }
}

#[cfg(test)]
fn do_collision_iteration(inp: &mut [Item], bias: f32) {
    let mut is = 0;
    let mut resolve_log = String::new();

    loop {
        resolve_log.push_str("------------------------------------------------------------\n");
        if resolve_step(&mut inp[..], bias, &mut resolve_log).is_some() {
            is += 1;
        }
        else { break }
        if is == 10 {
            println!("iteration limit, log:\n");
            println!("{}", resolve_log);
            panic!();
        }
    }

    println!("LOG:\n{}", resolve_log);
}

#[test]
fn test_resolve_collision() {
    let s1 = make_unit_item(0, 4f32, 0f32, 6f32, 4f32);
    let s2 = make_unit_item(1, 8f32, 3f32, 6f32, -1f32);
    let s3 = make_unit_item(2, 7f32, 7f32, 7f32, 5f32);
    let s4 = make_unit_item(3, 0f32, 4f32, 4f32, 4f32);
    let s5 = make_unit_item(4, 9f32, 8f32, 5f32, 8f32);
    let s6 = make_unit_item(5, 5f32, 8f32, 5f32, 8f32);

    let mut inp = [s1, s2, s3, s4, s5, s6];

    let res = [
        Vec2::new(5f32, 4f32),
        Vec2::new(7f32, -1f32),
        Vec2::new(7f32, 5f32),
        Vec2::new(3f32, 4f32),
        Vec2::new(9f32, 8f32),
        Vec2::new(5f32, 8f32),
    ];

    do_collision_iteration(&mut inp[..], ZERO);

    assert_eq!(true, inp.iter().zip(&res).all(|(a, b)| a.m.curr.x == b.x && a.m.curr.y == b.y));

    // TODO: add test for `hit` field
}

#[test]
fn test_resolve_collision2() {
    let orb_w = 15f32;
    let player_w = 16f32;
    let floor_h = 12.5f32;
    let player_h = 50f32;

    let player_cs = CollisionShape::Rect(player_w, player_h);
    let orb_cs = CollisionShape::Rect(orb_w, orb_w);
    let floor_cs = CollisionShape::Rect(400f32, floor_h);

    let down = Vec2::new(ZERO, -50f32);
    let left_down = Vec2::new(-10f32, -50f32);
    let right_down = Vec2::new(10f32, -50f32);

    //let origin = 96.98942f32;
    let origin = 100f32;

    let bias = 2f32;

    let left_player_pos = Vec2::new(origin, floor_h + floor_h + player_h + bias);
    let right_player_pos = math::add(&left_player_pos, &Vec2::new(player_w + orb_w + orb_w + player_w + bias, ZERO));
    let floor_pos = Vec2::new(400f32, floor_h);
    let orb_pos = Vec2::new(left_player_pos.x + player_w + orb_w, floor_h + floor_h + orb_w);

    let floor =    make_item(0, &floor_cs,  &floor_pos,        &floor_pos);
    let orb =      make_item(1, &orb_cs,    &orb_pos,          &math::add(&orb_pos, &down));
    let player_l = make_item(2, &player_cs, &left_player_pos,  &math::add(&left_player_pos, &right_down));
    let player_r = make_item(3, &player_cs, &right_player_pos, &math::add(&right_player_pos, &left_down));

    let mut inp = [floor, orb, player_l, player_r];

    do_collision_iteration(&mut inp[..], 0.01f32);

    assert_eq!(inp[2].hit, CollisionResponseInternal::StandsOnAndPushes(0, 1));
    assert_eq!(inp[3].hit, CollisionResponseInternal::StandsOnAndPushes(0, 1));
}

#[test]
fn test_resolve_collision3() {
    let player_pos =   Vec2::new(394.8707f32, 100.01f32);
    let floor_pos =    Vec2::new(400f32, 12.5f32);
    let platform_pos = Vec2::new(350f32, 37.5f32);
    let orb_pos =      Vec2::new(425.8807f32, 40.010002f32);

    let player_planned = Vec2::new(396.96667f32, 86.29181f32);
    let orb_planned =    Vec2::new(425.8807f32, 26.758446f32);

    let player =   make_item(0, &CollisionShape::Rect(16f32, 50f32),    &player_pos,   &player_planned);
    let floor =    make_item(1, &CollisionShape::Rect(400f32, 12.5f32), &floor_pos,    &floor_pos);
    let platform = make_item(2, &CollisionShape::Rect(50f32, 12.5f32),  &platform_pos, &platform_pos);
    let orb =      make_item(3, &CollisionShape::Rect(15f32, 15f32),    &orb_pos,      &orb_planned);

    let mut inp = [player, floor, platform, orb];

    do_collision_iteration(&mut inp[..], 0.01f32);

    assert_eq!(inp[0].hit, CollisionResponseInternal::StandsOnAndPushes(2, 3));
    assert_eq!(inp[3].hit, CollisionResponseInternal::StandsOnAndPushes(1, 0));
}

#[test]
fn test_resolve_collision4() {
    let platform = make_unit_item(1, ZERO, ZERO, ZERO, ZERO);
    let player = make_unit_item(0, -ONE, TWO, -3f32, -3f32);

    let mut inp = [player, platform];

    do_collision_iteration(&mut inp[..], 0.01f32);

    assert_eq!(inp[0].hit, CollisionResponseInternal::Free);
    assert_eq!(inp[1].hit, CollisionResponseInternal::Free);
}

pub struct MovementSystem {}

impl MovementSystem {
    pub fn new() -> Self {
        Self {}
    }

    pub fn turn(&mut self, es: &mut Entities) {
        // Apply planned position. (with possible modifications by collision system)
        for (_, e) in es.all_mut() {
            if let Entity {
                active: true,
                position: Some(PositionItem { ref mut origin, .. }),
                movement: Some(MovementItem { ref planned_position, .. }),
                ..
            } = *e
            {
                *origin = planned_position.clone();
            }
        }
    }
}
