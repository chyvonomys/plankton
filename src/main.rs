#[macro_use]
extern crate glium;
extern crate image;
#[cfg(feature = "audio")]
extern crate rodio;
extern crate time;
#[cfg(feature = "save-state")]
extern crate serde_json;
#[cfg(feature = "save-state")]
#[macro_use]
extern crate serde_derive;

#[cfg(not(feature = "save-state"))]
#[macro_use]
extern crate nosede;

const WIDTH: u32 = 800;
const HEIGHT: u32 = 500;

mod math {

    pub mod consts {
        pub const ZERO: f32 = 0.0f32;
        pub const HALF: f32 = 0.5f32;
        pub const ONE: f32 = 1.0f32;
        #[cfg(test)]
        pub const TWO: f32 = 2.0f32;
        pub const FOUR: f32 = 4.0f32;
        pub const HALFPI: f32 = ::std::f32::consts::FRAC_PI_2;
        pub const PI: f32 = ::std::f32::consts::PI;
        pub const TWOPI: f32 = PI + PI;
        pub const RAD2DEG: f32 = 180f32 / PI;
    }

    #[derive(Clone, Debug, Serialize, Deserialize)]
    pub struct Vec2 {
        pub x: f32,
        pub y: f32,
    }

    impl Default for Vec2 {
        #[inline]
        fn default() -> Self {
            Self { x: 0f32, y: 0f32 }
        }
    }

    impl Vec2 {
        #[inline]
        pub fn new(a: f32, b: f32) -> Self {
            Self { x: a, y: b }
        }

        #[inline]
        pub fn arr(&self) -> [f32; 2] {
            [self.x, self.y]
        }
    }

    #[inline]
    pub fn add(a: &Vec2, b: &Vec2) -> Vec2 {
        Vec2::new(a.x + b.x, a.y + b.y)
    }

    #[inline]
    pub fn sub(a: &Vec2, b: &Vec2) -> Vec2 {
        Vec2::new(a.x - b.x, a.y - b.y)
    }

    #[inline]
    pub fn mul(k: f32, v: &Vec2) -> Vec2 {
        Vec2::new(k * v.x, k * v.y)
    }

    #[inline]
    pub fn neg(v: &Vec2) -> Vec2 {
        Vec2::new(-v.x, -v.y)
    }

    // i --> j
    #[inline]
    pub fn perp(v: &Vec2) -> Vec2 {
        Vec2::new(-v.y, v.x)
    }

    #[inline]
    pub fn dot(a: &Vec2, b: &Vec2) -> f32 {
        a.x * b.x + a.y * b.y
    }

    #[inline]
    pub fn cross(a: &Vec2, b: &Vec2) -> f32 {
        a.x * b.y - a.y * b.x
    }

    #[inline]
    pub fn smoothstep(edge0: f32, edge1: f32, x: f32) -> f32 {
        let t = clamp((x - edge0) / (edge1 - edge0), 0f32, 1f32);
        t * t * (3f32 - 2f32 * t)
    }

    #[inline]
    pub fn clamp(from: f32, to: f32, x: f32) -> f32 {
        x.min(to).max(from)
    }

    #[inline]
    pub fn lerp(a: &Vec2, b: &Vec2, t: f32) -> Vec2 {
        add(a, &mul(t, &sub(b, a)))
    }

    #[inline]
    pub fn len(v: &Vec2) -> f32 {
        v.x.hypot(v.y)
    }

    #[inline]
    pub fn norm(v: &Vec2) -> Vec2 {
        mul(consts::ONE / len(v), v)
    }

    #[inline]
    pub fn rot(v: &Vec2, a: f32) -> Vec2 {
        let c = a.cos();
        let s = a.sin();
        Vec2::new(
            v.x * c - v.y * s,
            v.x * s + v.y * c,
        )
    }

    #[inline]
    pub fn ccw_angle(r0: &Vec2, r1: &Vec2) -> f32 {
        cross(&r0, &r1).atan2(dot(&r0, &r1))
    }

    pub fn intersect(p: &Vec2, pr: &Vec2, q: &Vec2, qs: &Vec2) -> Option<Vec2> {
        // pr = p + t * r
        // qs = q + u * s
        // p + t * r = q + u * s
        // (p + t * r) cross s = (q + u * s) cross s
        // t = (q - p) cross s / r cross s
        // (p + t * r) cross r = (q + u * s) cross r
        // u = (p - q) cross r / s cross r
        // u = (q - p) cross r / r cross s
        // r cross s == 0 => collinear

        let r = sub(&pr, &p);
        let s = sub(&qs, &q);

        let rxs = cross(&r, &s);

        if rxs.abs() < 0.0001f32 { None }
        else {
            let pq = sub(&q, &p);
            let t = cross(&pq, &s) / rxs;
            let u = cross(&pq, &r) / rxs;
            if t > 0.01f32 && t < 0.99f32 && u > 0.01f32 && u < 0.99f32 {
                Some(lerp(&p, &pr, t))
            } else { None }
        }
    }
}

mod screen;
mod audio;
mod sprite;
mod debug;
mod input;
mod animation;
mod collision;

fn main() {
    std::env::set_var("RUST_BACKTRACE", "1");

    #[cfg(feature = "audio")]
    let mut audio_system = audio::AudioSystem::new();

    let window = glium::glutin::WindowBuilder::new()
        .with_title("plankton")
        .with_dimensions((WIDTH, HEIGHT).into());
    let context = glium::glutin::ContextBuilder::new().with_vsync(true);
    let mut events_loop = glium::glutin::EventsLoop::new();

    if let Ok(display) = glium::Display::new(window, context, &events_loop) {
        let (fb_width, fb_height) = display.get_framebuffer_dimensions();
        println!("fb dimensions: {}x{}", fb_width, fb_height);

        let mut sprite_system = sprite::SpriteSystem::new(&display);
        let mut input_system = input::InputSystem::new(WIDTH, HEIGHT, WIDTH as f32 / fb_width as f32);
        let mut animation_system = animation::AnimationSystem::new();
        let mut screens = screen::Screens::new();
        let mut collision_system = collision::CollisionSystem::new();
        let mut movement_system = collision::MovementSystem::new();
        let mut debug_system = debug::DebugSystem::new(&display);

        let mut running = true;
        println!("entity size: {} bytes", std::mem::size_of::<screen::Entity>());

        while running {
            let now = time::precise_time_s();

            input_system.turn(now, &mut events_loop);

            running = screens.turn(&input_system.events);

            screens.stats.strings.clear();
            animation_system.turn(&screens.time_info, &mut screens.entities);

            collision_system.turn(&mut screens.entities);

            movement_system.turn(&mut screens.entities);

            #[cfg(feature = "audio")]
            audio_system.turn(&screens.entities);

            // drawing
            use glium::Surface; // frame has Surface trait
            let mut frame: glium::Frame = display.draw();
            frame.clear_color(0.0, 0.0, 0.0, 1.0);
            frame.clear_depth(1.0);

            sprite_system.turn(&screens.entities, &mut frame);
            debug_system.turn(&screens.time_info, &screens.entities, &mut frame, &input_system.events);

            if let Err(e) = frame.finish() {
                println!("Swap buffers error: {:?}", e);
                running = false;
            }
        }
        println!("Bye-bye!");
    }
}
