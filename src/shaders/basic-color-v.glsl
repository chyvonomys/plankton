#version 140
in vec2 i_ss_position;
in vec4 i_color;
uniform vec2 u_vp_size;
out vec4 l_color;
void main()
{
    gl_Position.xy = i_ss_position.xy / u_vp_size;
    gl_Position.xy -= vec2(0.5, 0.5);
    gl_Position.xy *= vec2(2.0, 2.0);
    gl_Position.z = 0.0f;
    gl_Position.w = 1.0f;
    l_color = i_color;
}
