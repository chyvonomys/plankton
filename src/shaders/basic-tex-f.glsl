#version 140
out vec4 o_color;
in vec2 l_uv;
in vec4 l_color;
uniform sampler2D u_sampler;
void main()
{
    o_color = l_color * texture(u_sampler, vec2(l_uv.x, 1.0f - l_uv.y));
    if (o_color.a < 1.0f)
        discard;
}
