#version 140
in vec3 i_ss_position;
in vec2 i_uv;
in vec4 i_color;
uniform vec2 u_vp_size;
out vec2 l_uv;
out vec4 l_color;
void main()
{
    gl_Position.xy = i_ss_position.xy / u_vp_size;
    gl_Position.xy -= vec2(0.5, 0.5);
    gl_Position.xy *= vec2(2.0, 2.0);
    gl_Position.z = i_ss_position.z;
    gl_Position.w = 1.0f;
    l_uv = i_uv;
    l_color = i_color;
}
