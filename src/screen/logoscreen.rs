use audio::{AudioItem, SoundId};
use input::{Action, InputInfo};
use screen::{Change, Entities, EntityCat, EntityId, Screen, ScreenId, Stats, TimeInfo};
use screen::{PositionItem, SizeItem};
use sprite::SpriteItem;
use math::{self, Vec2};

pub struct LogoScreen {
    digits: [EntityId; 5],
    logo: EntityId,
}

impl LogoScreen {
    pub fn new(ss: &mut Entities) -> Self {
        Self {
            digits: [
                ss.create(EntityCat::Volatile),
                ss.create(EntityCat::Volatile),
                ss.create(EntityCat::Volatile),
                ss.create(EntityCat::Volatile),
                ss.create(EntityCat::Volatile),
            ],
            logo: ss.create(EntityCat::Volatile),
        }
    }
}

mod utils {
    pub fn number_to_digits(x: f32) -> [u32; 5] {
        let mut p = 100.0;
        let mut res = [0; 5];
        for i in 0..5 {
            let d = (x * p) as u32 % 10;
            res[4 - i] = d;
            p /= 10.0;
        }
        res
    }

    #[cfg(test)]
    #[test]
    fn n2d_test() {
        assert_eq!(number_to_digits(0.0), [0, 0, 0, 0, 0]);
        assert_eq!(number_to_digits(12.0), [0, 1, 2, 0, 0]);
        assert_eq!(number_to_digits(123.45), [1, 2, 3, 4, 5]);
        assert_eq!(number_to_digits(6123.456), [1, 2, 3, 4, 5]);
    }
}

impl Screen for LogoScreen {
    fn update(
        &mut self,
        sid: ScreenId,
        tinfo: &TimeInfo,
        iinfo: &InputInfo,
        es: &mut Entities,
        _: &Stats,
    ) -> Change {
        // TODO:??
        if sid != ScreenId::Logo {
            return Change::Pop;
        }

        let dur = tinfo.get_total_seconds();

        if let Some(logo) = es.get_mut(self.logo) {
            logo.active = true;
            if dur < 3.0f32 {
                logo.audio = Some(AudioItem {
                    id: SoundId::d3note1sec(),
                    volume: math::smoothstep(0.2f32, 1.0f32, dur) * 0.5f32,
                });
            } else {
                logo.audio = None;
            }
        }

        const W: f32 = 64.0f32;
        const H: f32 = 128.0f32;

        for (i, d) in utils::number_to_digits(dur * 10.0f32).iter().enumerate() {
            if let Some(digit) = es.get_mut(self.digits[i]) {
                digit.active = true;
                digit.position = Some(PositionItem::new(&Vec2::new(
                    W * (i as f32 + 0.5f32),
                    0.5f32 * H + dur * 10.0f32,
                )));
                digit.size = Some(SizeItem {
                    width: W,
                    height: H,
                });
                digit.sprite = Some(SpriteItem::digit(*d));
            }
        }

        const NEXT: Change = Change::Swap(ScreenId::Title);
        if dur > 3.0f32 {
            NEXT
        } else {
            match iinfo.action.last() {
                Some(&(Action::Release, t)) if tinfo.is_recent(t) => NEXT,
                _ => Change::Stay,
            }
        }
    }
}
