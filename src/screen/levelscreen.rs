use screen::{Change, Entities, Entity, EntityCat, EntityId, Screen, ScreenId, Stats, TimeInfo};
use input::InputInfo;
use screen::{PathItem, PositionItem, SizeItem};
use audio::{AudioItem, SoundId};
use animation::{AnimationItem, SashaItem, SashaInput, OrbInput, LeftInput, RightInput, JumpInput, CarryInput};
use sprite::SpriteItem;
use collision::{CollisionItem, MovementItem, CollisionResponse};
use math::{self, Vec2};
use math::consts::*;

enum Wall { Left, Platform, Right }

struct LevelDesc {
    orbs: &'static [(u32, u32)], // (x, y)
    links: &'static [(usize, usize)], // (orb_id0, orb_id1)
    possible: &'static [(usize, usize, usize, usize)], // (link0orb0 link0orb1 link1orb0 link1orb1) TODO: calc automatically
    walls: &'static [(u32, u32, u32, u32, Wall)], // (x0, y0, width, height)
    door: (u32, u32),
}

const ORB_R: f32 = 15f32;
const ORB_D: f32 = ORB_R + ORB_R;

const LEVELS: LevelDesc = LevelDesc {
    orbs: &[(100, 375), (500, 325), (350, 75), (550, 175)],
    links: &[(0, 1), (0, 3), (0, 2), (1, 2), (1, 3), (2, 3)],
    // TODO: this should not be hardcoded
    possible: &[(0, 3, 1, 2), (0, 2, 1, 3), (0, 1, 2, 3)],
    walls: &[
        (0,   0,   800, 25, Wall::Platform), // floor
        (50,  325, 100, 25, Wall::Platform), // #0
        (450, 275, 100, 25, Wall::Platform), // #1
        (300, 25,  100, 25, Wall::Platform), // #2
        (500, 130, 100, 25, Wall::Platform), // #3
        (250, 175, 100, 25, Wall::Platform), // target
        (650, 225, 100, 25, Wall::Platform), // help
        (0,   30,  25, 400, Wall::Left), // left end
        (775, 30,  25, 400, Wall::Right), // right end
    ],
    door: (700, 25+64),
};

pub struct LevelScreen {
    player: EntityId,
    enemy: EntityId,
    enemy_left: bool,
    carried: Option<EntityId>,
    obstacles: Vec<EntityId>,
    text: Vec<EntityId>,
    level: Vec<EntityId>,
    orbs: Vec<EntityId>,
    links: Vec<(EntityId, EntityId, EntityId)>,
    possible: Vec<(EntityId, EntityId, EntityId, EntityId, EntityId)>,
    door: EntityId,
    bg: EntityId,
}

impl LevelScreen {
    fn text_ent(&self, r: usize, c: usize) -> Option<EntityId> {
        self.text.get(r * 50 + c).map(|v| *v)
    }

    #[inline]
    fn is_orb(&self, eid: EntityId) -> bool {
        self.orbs.contains(&eid)
    }

    pub fn new(_level: usize, es: &mut Entities) -> Self {
        let player_pos = Vec2::new(200.0f32, 25f32 + 50f32 + 50f32);
        let enemy_pos = Vec2::new(700f32, 330f32);
        let screen_center = Vec2::new(400.0f32, 250.0f32);

        let pid = {
            let (id, player) = es.create_get(EntityCat::Persistent);
            let mut pi = PositionItem::new(&player_pos);
            pi.layer = 0.0f32;
            player.position = Some(pi);
            player.size = Some(SizeItem {
                width: 128.0f32,
                height: 128.0f32,
            });
            player.animation = Some(AnimationItem::sasha());
            player.movement = Some(MovementItem::new(&player_pos));
            player.collision = Some(CollisionItem::rect(16f32, 50f32));
            id
        };

        let enemyid = {
            let (id, player) = es.create_get(EntityCat::Persistent);
            let mut pi = PositionItem::new(&enemy_pos);
            pi.layer = 0.0f32;
            player.position = Some(pi);
            player.size = Some(SizeItem {
                width: 128.0f32,
                height: 128.0f32,
            });
            player.animation = Some(AnimationItem::sasha());
            player.movement = Some(MovementItem::new(&player_pos));
            player.collision = Some(CollisionItem::rect(16f32, 50f32));
            id
        };

        let bgid = {
            let (id, bg) = es.create_get(EntityCat::Persistent);
            bg.audio = Some(AudioItem {
                id: SoundId::level_music(),
                volume: 0.5f32,
            });
            bg.position = Some(PositionItem::new(&screen_center));
            bg.size = Some(SizeItem {
                width: 800.0f32,
                height: 500.0f32,
            });
            bg.sprite = Some(SpriteItem::background());
            id
        };

        const CW: f32 = 8f32;
        const CH: f32 = 16f32;
        let chars = (0..300).map(|i| {
            let (id, e) = es.create_get(EntityCat::Volatile);
            e.size = Some(SizeItem { width: CW, height: CH });
            e.position = Some(PositionItem::new(&Vec2::new(
                CW * ((i % 50) as f32 + 0.5f32),
                500f32 - CH * ((i / 50) as f32 + 0.5f32),
            )));
            id
        }).collect();

        let walls = LEVELS.walls.iter().map(|w| {
            let (id, e) = es.create_get(EntityCat::Persistent);
            let width = w.2 as f32;
            let height = w.3 as f32;
            let x0 = w.0 as f32 + width * 0.5f32;
            let y0 = w.1 as f32 + height * 0.5f32; 
            e.position = Some(PositionItem::new(&Vec2::new(x0, y0)));
            e.size = Some(SizeItem { width, height });
            e.sprite = Some(match w.4 {
                Wall::Left => SpriteItem::left_end(),
                Wall::Right => SpriteItem::right_end(),
                Wall::Platform => SpriteItem::blank_wall(),
            });
            e.collision = Some(CollisionItem::rect(width * 0.5f32, height * 0.5f32));
            id
        }).collect();

        let orb_size = Some(SizeItem { width: ORB_D, height: ORB_D });
        
        let orbs: Vec<EntityId> = LEVELS.orbs.iter().map(|o| {
            let (id, e) = es.create_get(EntityCat::Persistent);
            let pos = Vec2::new(o.0 as f32, o.1 as f32);
            e.animation = Some(AnimationItem::orb());
            e.position = Some(PositionItem::new(&pos));
            e.movement = Some(MovementItem::new(&pos));
            e.collision = Some(CollisionItem::rect(ORB_R, ORB_R));
            e.size = orb_size.clone();
            e.sprite = Some(SpriteItem::orb());
            id
        }).collect();

        let links = LEVELS.links.iter().map(|l| {
            let (id, e) = es.create_get(EntityCat::Persistent);
            let points = [l.0, l.1].iter().map(|i| {
                let p = LEVELS.orbs[*i as usize];
                Vec2::new(p.0 as f32, p.1 as f32)
            }).collect();
            e.path = Some(PathItem { points });
            (id, orbs[l.0], orbs[l.1])
        }).collect();

        let possible = LEVELS.possible.iter().map(|&(l0o0, l0o1, l1o0, l1o1)| {
            let (id, e) = es.create_get(EntityCat::Persistent);
            e.size = Some(SizeItem { width: 20f32, height: 20f32 });
            e.sprite = Some(SpriteItem::logo());
            (id, orbs[l0o0], orbs[l0o1], orbs[l1o0], orbs[l1o1])
        }).collect();

        let door = {
            let (id, e) = es.create_get(EntityCat::Persistent);
            e.size = Some(SizeItem { width: 128f32, height: 128f32 });
            e.position = Some(PositionItem::new(&Vec2::new(LEVELS.door.0 as f32, LEVELS.door.1 as f32)));
            id
        };

        Self {
            player: pid,
            enemy: enemyid,
            enemy_left: true,
            carried: None,
            obstacles: walls,
            text: chars,
            level: Vec::new(),
            orbs,
            links,
            possible,
            door,
            bg: bgid,
        }
    }
}

impl Screen for LevelScreen {
    fn update(&mut self, sid: ScreenId, _: &TimeInfo, iinfo: &InputInfo, es: &mut Entities, stats: &Stats)
    -> Change {

        if !(match sid {
            ScreenId::Level(_) => true,
            _ => false
        }) { return Change::Pop }

        es.activate(self.player);
        es.activate(self.enemy);
        es.activate(self.bg);
        es.activate_mult(&self.obstacles);
        es.activate_mult(&self.orbs);
        es.activate_mult(&self.level);
        es.activate(self.door);

        for &(eid, _, _) in &self.links {
            es.activate(eid);
        }

        let mut pos = None;
        let mut coll_resp = None;

        let snapshot = iinfo.get_snapshot();

        if let Some(&mut Entity {
            animation: Some(AnimationItem::Sasha(SashaItem { ref mut input, .. })),
            collision: Some(ref citem),
            position: Some(PositionItem { ref origin, .. }),
            ..
        }) = es.get_mut(self.player)
        {
            pos = Some(origin.clone());

            coll_resp = Some(citem.get_response().clone());

            *input = SashaInput(
                if snapshot.left { LeftInput::L1 } else { LeftInput::L0 },
                if snapshot.right { RightInput::R1 } else { RightInput::R0 },
                if snapshot.jump { JumpInput::J1 } else { JumpInput::J0 },
            );
        }

        if let Some(&mut Entity {
            animation: Some(AnimationItem::Sasha(SashaItem { ref mut input, .. })),
            collision: Some(ref citem),
            ..
        }) = es.get_mut(self.enemy)
        {
            let turn = match (self.enemy_left, citem.get_response()) {
                (true, &CollisionResponse::PushesLeft(_)) => true,
                (false, &CollisionResponse::PushesRight(_)) => true,
                _ => false,
            };

            if turn {
                self.enemy_left = !self.enemy_left;
            }

            *input = SashaInput(
                if self.enemy_left { LeftInput::L1 } else { LeftInput::L0 },
                if !self.enemy_left { RightInput::R1 } else { RightInput::R0 },
                if turn { JumpInput::J0 } else { JumpInput::J1 },
            );
        }

        let pick = coll_resp.as_ref().and_then(|cr| cr.get_pushes())
            .and_then(|eid| if self.is_orb(eid) { Some(eid) } else { None });

        self.carried = match (pick, self.carried, snapshot.action) {
            // drop
            (_, Some(cid), false) => {
                let c = es.get_mut(cid).unwrap();
                c.collision = Some(CollisionItem::rect(ORB_R, ORB_R));
                if let Some(AnimationItem::Orb(ref mut orbitem)) = c.animation {
                    orbitem.input = OrbInput(CarryInput::C0);
                }
                None
            },
            (_, None, false) |
            (None, None, true) => None,
            // keep
            (_, Some(cid), true) => Some(cid),
            // pick up
            (Some(oid), None, true) => {
                let c = es.get_mut(oid).unwrap();
                c.collision = None;
                if let Some(AnimationItem::Orb(ref mut orbitem)) = c.animation {
                    orbitem.input = OrbInput(CarryInput::C1);
                }
                Some(oid)
            },
        };
        
        // Update carried orb position with player's position.
        if let Some(player_pos) = es.get_pos(self.player) {
            let e: Option<&mut Entity> = self.carried.and_then(|cid| es.get_mut(cid));
            let pi: Option<&mut PositionItem> = e.and_then(|e| e.position.as_mut());
            let ori: Option<&mut Vec2> = pi.map(|p| &mut p.origin);

            if let Some(o) = ori {
                *o = player_pos;
            }
        }
        
        // Update links with actual orb positions
        for &(lid, o1id, o2id) in &self.links {
            let o1pos = es.get_pos(o1id).unwrap();
            let o2pos = es.get_pos(o2id).unwrap();
            let pts = es.get_mut(lid).and_then(|e| e.path.as_mut()).map(|p| &mut p.points).unwrap();
            pts.clear();
            pts.push(o1pos);
            pts.push(o2pos);
        }

        let mut activate_door = true;

        // Check for intersecting links
        for p in &self.possible {
            let p00 = es.get_pos(p.1).unwrap();
            let p01 = es.get_pos(p.2).unwrap();
            let p10 = es.get_pos(p.3).unwrap();
            let p11 = es.get_pos(p.4).unwrap();
            if let Some(x) = math::intersect(&p00, &p01, &p10, &p11) {
                let ei = es.get_mut(p.0).unwrap();
                ei.active = true;
                ei.position = Some(PositionItem::new(&x));
                activate_door = false;
            }
        }

        es.get_mut(self.door).map(
            |e| e.sprite = Some(if activate_door {
                SpriteItem::exit_door()
            } else {
                SpriteItem::room_door()
            }
        ));

        let ccw_angle = math::ccw_angle(
            &Vec2::new(ZERO, -ONE),
            &math::sub(&iinfo.mouse_pos, &Vec2::new(400f32, 250f32)),
        );

        let strings = [
            format!("pos: {:?}", pos),
            format!("coll: {:?}", coll_resp),
            format!("mouse pos: {:?}, ccw_angle: {}", iinfo.mouse_pos, ccw_angle * RAD2DEG),
        ];

        for (r, s) in stats.strings.iter().chain(strings.iter().take(3)).enumerate() {
            for (i, c) in s.chars().enumerate() {
                let e = self.text_ent(r, i).and_then(|eid| es.get_mut(eid));
                if let Some(ch) = e {
                    ch.active = true;
                    ch.sprite = if c >= ' ' && c < '~' {
                        Some(SpriteItem::character(c as u8))
                    } else {
                        Some(SpriteItem::character(0u8))
                    };
                }
            }
        }     

        Change::Stay
    }
}
