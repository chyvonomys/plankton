use animation::{AnimationItem, SashaItem, SashaInput, LeftInput, RightInput, JumpInput};
use audio::{AudioItem, SoundId};
use input::{Action, InputInfo};
use screen::{Change, Entities, EntityCat, EntityId, Screen, ScreenId, Stats, TimeInfo};
use screen::{PositionItem, SizeItem};
use math::{self, Vec2};

pub struct TitleScreen {
    walking: EntityId,
}

impl TitleScreen {
    pub fn new(es: &mut Entities) -> Self {
        let wid = es.create(EntityCat::Persistent);
        if let Some(walking) = es.get_mut(wid) {
            walking.position = Some(PositionItem::new(&Vec2::new(64.0f32, 64.0f32)));
            walking.animation = Some(AnimationItem::sasha());
            walking.size = Some(SizeItem {
                width: 128.0f32,
                height: 128.0f32,
            });
        }

        Self { walking: wid }
    }
}

impl Screen for TitleScreen {
    fn update(
        &mut self, sid: ScreenId, tinfo: &TimeInfo, iinfo: &InputInfo, es: &mut Entities, _: &Stats,
    ) -> Change {

        if sid != ScreenId::Title {
            return Change::Pop;
        }

        if let Some(e) = es.get_mut(self.walking) {

            e.active = true;
            e.audio = Some(AudioItem {
                id: SoundId::title_music(),
                volume: math::smoothstep(0.2f32, 1.0f32, tinfo.get_total_seconds()) * 0.5f32,
            });

            if let Some(AnimationItem::Sasha(SashaItem {
                ref mut input, ..
            })) = e.animation {
                let snapshot = iinfo.get_snapshot();
                *input = SashaInput(
                    if snapshot.left { LeftInput::L1 } else { LeftInput::L0 },
                    if snapshot.right { RightInput::R1 } else { RightInput::R0 },
                    if snapshot.jump { JumpInput::J1 } else { JumpInput::J0 },
                );
            }
        }

        match iinfo.up.last() {
            Some(&(Action::Release, t)) if tinfo.is_recent(t) => Change::Swap(ScreenId::Level(1)),
            _ => Change::Stay,
        }
    }
}
