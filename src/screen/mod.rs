use input::{Action, InputInfo};
use time;
#[cfg(feature = "save-state")]
use serde_json;
use math::Vec2;
use std::collections::HashMap;

pub struct TimeInfo {
    init_time: f64,
    prev_upd_time: f64,
    curr_upd_time: f64,
}

impl TimeInfo {
    pub fn new(t: f64) -> Self {
        Self {
            init_time: t,
            prev_upd_time: t,
            curr_upd_time: t,
        }
    }
    pub fn get_delta_seconds(&self) -> f32 {
        (self.curr_upd_time - self.prev_upd_time) as f32
    }

    pub fn get_total_seconds(&self) -> f32 {
        (self.curr_upd_time - self.init_time) as f32
    }

    pub fn update(&mut self, t: f64) {
        self.prev_upd_time = self.curr_upd_time;
        self.curr_upd_time = t;
    }

    pub fn is_recent(&self, t: f64) -> bool {
        t - self.prev_upd_time > 0.0f64
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PositionItem {
    pub origin: Vec2,
    pub layer: f32,
}

impl PositionItem {
    pub fn new(p: &Vec2) -> Self {
        Self {
            origin: p.clone(),
            layer: 1.0f32,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PathItem {
    pub points: Vec<Vec2>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct SizeItem {
    pub width: f32,
    pub height: f32,
}

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug, Serialize, Deserialize)]
pub enum EntityCat { Persistent, Volatile }

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug, Serialize, Deserialize)]
pub struct EntityId(usize, EntityCat);

#[cfg(test)]
pub fn make_test_eid(id: usize) -> EntityId {
    EntityId(id, EntityCat::Volatile)
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Entity {
    pub active: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub position: Option<PositionItem>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub movement: Option<::collision::MovementItem>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub collision: Option<::collision::CollisionItem>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub size: Option<SizeItem>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sprite: Option<::sprite::SpriteItem>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub audio: Option<::audio::AudioItem>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub path: Option<PathItem>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub animation: Option<::animation::AnimationItem>,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub enum ScreenId {
    Logo,
    Title,
    Level(u32),
}

pub enum Change {
    Stay,
    Swap(ScreenId),
    //    Push(ScreenId),
    Pop,
}

pub trait Screen {
    fn update(&mut self, ScreenId, &TimeInfo, &InputInfo, &mut Entities, &Stats) -> Change {
        Change::Pop
    }
}

mod logoscreen;
mod titlescreen;
mod levelscreen;

#[derive(Default)]
pub struct Stats {
    pub strings: Vec<String>,
}

pub struct Entities {
    persistent: Vec<Entity>,
    volatile: Vec<Entity>,
}

impl Entities {
    pub fn new() -> Self {
        Self {
            persistent: Vec::new(),
            volatile: Vec::new(),
        }
    }

    pub fn create(&mut self, cat: EntityCat) -> EntityId {
        self.create_get(cat).0
    }

    pub fn create_get(&mut self, cat: EntityCat) -> (EntityId, &mut Entity) {
        let storage = match cat {
            EntityCat::Persistent => &mut self.persistent,
            EntityCat::Volatile => &mut self.volatile,
        };

        let eid = EntityId(storage.len(), cat);
        let e = Entity {
            active: false,
            position: None,
            movement: None,
            collision: None,
            size: None,
            sprite: None,
            audio: None,
            path: None,
            animation: None,
        };
        storage.push(e);
        (eid, storage.last_mut().unwrap())
    }

    #[inline]
    pub fn get_mut(&mut self, eid: EntityId) -> Option<&mut Entity> {
        match eid.1 {
            EntityCat::Persistent => &mut self.persistent,
            EntityCat::Volatile => &mut self.volatile,
        }.get_mut(eid.0)
    }

    #[inline]
    pub fn get(&self, eid: EntityId) -> Option<&Entity> {
        match eid.1 {
            EntityCat::Persistent => &self.persistent,
            EntityCat::Volatile => &self.volatile,
        }.get(eid.0)
    }

    #[inline]
    pub fn all(&self) -> impl Iterator<Item = (EntityId, &Entity)> {
        let pi = self.persistent.iter().enumerate().map(|p| (EntityId(p.0, EntityCat::Persistent), p.1));
        let vi = self.volatile.iter().enumerate().map(|p| (EntityId(p.0, EntityCat::Volatile), p.1));
        pi.chain(vi)
    }

    #[inline]
    pub fn all_mut(&mut self) -> impl Iterator<Item = (EntityId, &mut Entity)> {
        let pi = self.persistent.iter_mut().enumerate().map(|p| (EntityId(p.0, EntityCat::Persistent), p.1));
        let vi = self.volatile.iter_mut().enumerate().map(|p| (EntityId(p.0, EntityCat::Volatile), p.1));
        pi.chain(vi)
    }

    #[inline]
    pub fn activate(&mut self, id: EntityId) {
        self.get_mut(id).map(|e| e.active = true);
    }

    #[inline]
    pub fn activate_mult(&mut self, ids: &[EntityId]) {
        for id in ids {
            self.activate(*id);
        }
    }

    #[inline]
    pub fn get_pos(&self, id: EntityId) -> Option<Vec2> {
        let e: Option<&Entity> = self.get(id);
        let pos_item: Option<&PositionItem> = e.and_then(|e| e.position.as_ref());
        let ppos: Option<Vec2> = pos_item.map(|p| p.origin.clone());
        ppos
    }
}

pub struct Screens {
    screens: HashMap<ScreenId, Box<Screen>>,
    stack: Vec<ScreenId>,
    pub entities: Entities,
    pub time_info: TimeInfo,
    pub stats: Stats,
}

#[cfg(feature = "save-state")]
const ES_FILE: &'static str = "es.json";

impl Screens {
    pub fn new() -> Self {
        let mut es = Entities::new();
        let los: Box<Screen> = Box::new(logoscreen::LogoScreen::new(&mut es));
        let tis: Box<Screen> = Box::new(titlescreen::TitleScreen::new(&mut es));
        let les: Box<Screen> = Box::new(levelscreen::LevelScreen::new(0, &mut es));
        let pus: Box<Screen> = Box::new(levelscreen::LevelScreen::new(1, &mut es));

        let mut screens = HashMap::new();
        screens.insert(ScreenId::Logo, los);
        screens.insert(ScreenId::Title, tis);
        screens.insert(ScreenId::Level(0), les);
        screens.insert(ScreenId::Level(1), pus);

        Self {
            screens,
            stack: vec![ScreenId::Logo],
            entities: es,
            time_info: TimeInfo::new(time::precise_time_s()),
            stats: Stats::default(),
        }
    }

    fn save_entities(&self) {
        #[cfg(feature = "save-state")]
        match ::std::fs::File::create(ES_FILE) {
            Ok(f) => {
                let w = ::std::io::BufWriter::new(f);
                match serde_json::to_writer_pretty(w, &self.entities.persistent) {
                    Ok(()) => println!("saved entities to '{}'", ES_FILE),
                    Err(e) => println!("failed saving entities: {}", e.to_string()),
                }
            },
            Err(e) => println!("failed opening file '{}': {}", ES_FILE, e.to_string()),
        }
        #[cfg(not(feature = "save-state"))]
        println!("save/load state is disabled");
    }

    fn load_entities(&mut self) {
        #[cfg(feature = "save-state")]
        match ::std::fs::File::open(ES_FILE) {
            Ok(f) => {
                let r = ::std::io::BufReader::new(f);
                match serde_json::from_reader::<_, Vec<Entity>>(r) {
                    Ok(es) => {
                        self.entities.persistent = es;
                        println!("loaded and hotswapped entities");
                    },
                    Err(e) => println!("failed loading entities: {}", e.to_string()),
                }
            },
            Err(e) => println!("failed opening file '{}': {}", ES_FILE, e.to_string()),
        }
        #[cfg(not(feature = "save-state"))]
        println!("save/load state is disabled");
    }

    pub fn turn(&mut self, iinfo: &InputInfo) -> bool {
        let now = time::precise_time_s();

        self.time_info.update(now);

        for (_, e) in self.entities.all_mut() {
            e.active = false;
        }

        let sid = *self.stack.last().unwrap();

        if let Some(scr) = self.screens.get_mut(&sid) {
            match scr.update(sid, &self.time_info, iinfo, &mut self.entities, &self.stats) {
                Change::Stay => {}
                Change::Swap(newsid) => {
                    self.stack.pop();
                    self.stack.push(newsid);
                    self.time_info = TimeInfo::new(now);
                }
                /*
                Change::Push(newsid) => {
                    self.stack.push(newsid);
                },
                */
                Change::Pop => {
                    self.stack.pop();
                    self.time_info = TimeInfo::new(now);
                }
            }
        }

        if let Some(&(Action::Press, t)) = iinfo.save.last() {
            if self.time_info.is_recent(t) {
                self.save_entities();
            }
        }

        if let Some(&(Action::Press, t)) = iinfo.load.last() {
            if self.time_info.is_recent(t) {
                self.load_entities();
            }
        }

        let shutdown = self.stack.is_empty() || iinfo.shutdown;
        !shutdown
    }
}
