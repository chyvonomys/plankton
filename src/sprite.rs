use glium;
use image;
use screen::{Entities, Entity};
use glium::texture::{RawImage2d, Texture2d};
use glium::backend::Facade;
use glium::Surface;
use math::consts::*;

#[derive(Copy, Clone)]
struct Vtx2P2T4C {
    i_ss_position: [f32; 3],
    i_uv: [f32; 2],
    i_color: [f32; 4],
}

implement_vertex!(Vtx2P2T4C, i_ss_position, i_uv, i_color);

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct SpriteId(usize); // creation is private to this mod

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct SpriteItem {
    pub id: SpriteId,
    pub u0: f32,
    pub u1: f32,
    pub v0: f32,
    pub v1: f32,
}

impl SpriteItem {
    fn sprite(id: usize) -> Self {
        Self {
            id: SpriteId(id),
            u0: ZERO,
            u1: ONE,
            v0: ZERO,
            v1: ONE,
        }
    }

    fn tilerow_item(id: usize, c: u32, cs: u32) -> Self {
        let w = ONE / cs as f32;
        Self {
            id: SpriteId(id),
            u0: c as f32 * w,
            u1: (c as f32 + ONE) * w,
            v0: ZERO,
            v1: ONE,
        }
    }

    fn tilerowcol_item(id: usize, c: u32, r: u32, cs: u32, rs: u32) -> Self {
        let w = ONE / cs as f32;
        let h = ONE / rs as f32;
        Self {
            id: SpriteId(id),
            u0: c as f32 * w,
            u1: (c as f32 + ONE) * w,
            v0: r as f32 * h,
            v1: (r as f32 + ONE) * h,
        }
    }

    pub fn background() -> Self {
        Self::sprite(0)
    }

    pub fn logo() -> Self {
        Self::sprite(1)
    }

    pub fn sasha(f: u32, mirror: bool) -> Self {
        let ff = (f % 12) as f32;
        let w = ONE / 12.0f32;
        let h = ONE;
        Self {
            id: SpriteId(2),
            u0: if mirror { ff * w } else { (ff + ONE) * w },
            u1: if mirror { (ff + ONE) * w } else { ff * w },
            v0: ZERO,
            v1: h,
        }
    }

    pub fn digit(d: u32) -> Self {
        Self::tilerow_item(3, d, 10)
    }

    pub fn room_door() -> Self {
        Self::tilerow_item(4, 0, 5)
    }

    pub fn exit_door() -> Self {
        Self::tilerow_item(4, 1, 5)
    }

    pub fn blank_wall() -> Self {
        Self::tilerow_item(4, 2, 5)
    }

    pub fn left_end() -> Self {
        Self::tilerow_item(4, 3, 5)
    }

    pub fn right_end() -> Self {
        Self::tilerow_item(4, 4, 5)
    }

    pub fn character(c: u8) -> Self {
        Self::tilerowcol_item(5, u32::from(c % 16), u32::from(15 - c / 16), 16, 16)
    }

    pub fn orb() -> Self {
        Self::sprite(6)
    }
}

struct Batch {
    texture: Texture2d,
    bucket: Vec<Vtx2P2T4C>,
}

pub struct SpriteSystem {
    batches: Vec<Batch>,
    program: glium::Program,
    vb: glium::VertexBuffer<Vtx2P2T4C>,
    ib: glium::IndexBuffer<u16>,
}

enum ImageSource {
    PNG,
    RawIndexed(u32), // 8bit per pixel, given width
}

const GREEN: &[u8; 4] = &[0x00u8, 0xFFu8, 0x00u8, 0xFFu8];
const BLACK: &[u8; 4] = &[0x00u8, 0x00u8, 0x00u8, 0xFFu8];

impl SpriteSystem {

    #[inline]
    fn palette(b: u8) -> &'static [u8; 4] {
        if b == 0x01u8 { 
            GREEN
        } else {
            BLACK
        }
    }

    fn make_image<'a>(bytes: &[u8], src: &ImageSource) -> RawImage2d<'a, u8> {
        match *src {
            ImageSource::PNG => {
                let i = image::load(::std::io::Cursor::new(bytes), image::PNG)
                    .unwrap()
                    .to_rgba();
                let d = i.dimensions();
                RawImage2d::from_raw_rgba(i.into_raw(), d)
            },
            ImageSource::RawIndexed(width) => {
                let rgba: Vec<u8> = bytes.iter()
                    .flat_map(|b| Self::palette(*b))
                    .cloned().collect();
                RawImage2d::from_raw_rgba(rgba, (width, bytes.len() as u32 / width))
            }
        }
    }

    pub fn new(display: &glium::Display) -> Self {
        let bg_bs = (&include_bytes!("../resources/background.png")[..], ImageSource::PNG);
        let logo_bs = (&include_bytes!("../resources/logo.png")[..], ImageSource::PNG);
        let sasha_bs = (&include_bytes!("../resources/sasha2.png")[..], ImageSource::PNG);
        let digits_bs = (&include_bytes!("../resources/0-9.png")[..], ImageSource::PNG);
        let wall_bs = (&include_bytes!("../resources/walls.png")[..], ImageSource::PNG);
        let font_bs = (&include_bytes!("../resources/font1.gt")[..], ImageSource::RawIndexed(16 * 8));
        let orb_bs = (&include_bytes!("../resources/orb.png")[..], ImageSource::PNG);

        let vert_src = include_str!("shaders/basic-tex-v.glsl");
        let frag_src = include_str!("shaders/basic-tex-f.glsl");

        let facade = display.get_context();

        /*

        2--3
        | /|
        |/ |
        0--1

        */

        const NUM_QUADS: usize = 1000;

        let mut quad_indexes: Vec<u16> = Vec::new();

        for qi in 0..NUM_QUADS as u16 {
            let i = qi * 4;
            quad_indexes.extend_from_slice(&[i, i + 2, i + 3, i + 3, i + 1, i]);
        }

/*
        let qis: Vec<u16> = (0..NUM_QUADS as u16)
            .flat_map(|i| [i, i + 2, i + 3, i + 3, i + 1, i].iter())
            .cloned().collect();
*/

        let program = glium::Program::new(
            facade,
            glium::program::ProgramCreationInput::SourceCode {
                vertex_shader: vert_src,
                tessellation_control_shader: None,
                tessellation_evaluation_shader: None,
                geometry_shader: None,
                fragment_shader: frag_src,
                transform_feedback_varyings: None,
                outputs_srgb: true, // This tells glium not to do gamma correction.
                uses_point_size: false,
            },
        ).unwrap();

        Self {
            batches: [bg_bs, logo_bs, sasha_bs, digits_bs, wall_bs, font_bs, orb_bs]
                .iter()
                .map(|p| {
                    Batch {
                        texture: Texture2d::new(facade, Self::make_image(p.0, &p.1)).unwrap(),
                        bucket: Vec::new(),
                    }
                })
                .collect(),
            program,
            vb: glium::VertexBuffer::empty_persistent(facade, NUM_QUADS * 4).unwrap(),
            ib: glium::index::IndexBuffer::immutable(
                facade,
                glium::index::PrimitiveType::TrianglesList,
                &quad_indexes,
            ).unwrap(),
        }
    }

    pub fn turn(&mut self, es: &Entities, frame: &mut glium::Frame) {
        for b in &mut self.batches {
            b.bucket.clear();
        }

        for (_, e) in es.all() {
            if let Entity {
                active: true,
                position: Some(ref p),
                size: Some(ref si),
                sprite:
                    Some(SpriteItem {
                        id: SpriteId(sid),
                        u0,
                        u1,
                        v0,
                        v1,
                    }),
                ..
            } = *e
            {
                let ox = p.origin.x;
                let oy = p.origin.y;
                let rx = si.width * 0.5f32;
                let ry = si.height * 0.5f32;

                let bl = [ox - rx, oy - ry, p.layer];
                let tl = [ox - rx, oy + ry, p.layer];
                let br = [ox + rx, oy - ry, p.layer];
                let tr = [ox + rx, oy + ry, p.layer];
                let white = [ONE, ONE, ONE, ONE];

                let b = &mut self.batches[sid].bucket;

                b.push(Vtx2P2T4C {
                    i_ss_position: bl,
                    i_uv: [u0, v0],
                    i_color: white,
                });
                b.push(Vtx2P2T4C {
                    i_ss_position: tl,
                    i_uv: [u0, v1],
                    i_color: white,
                });
                b.push(Vtx2P2T4C {
                    i_ss_position: br,
                    i_uv: [u1, v0],
                    i_color: white,
                });
                b.push(Vtx2P2T4C {
                    i_ss_position: tr,
                    i_uv: [u1, v1],
                    i_color: white,
                });
            }
        }

        //let (w, h) = frame.get_dimensions();
        let (w, h) = (800, 500);
        let dim: [f32; 2] = [w as f32, h as f32];
        let mut params = glium::DrawParameters::default();
        //params.blend = glium::Blend::alpha_blending();
        params.depth = glium::Depth {
            test: glium::draw_parameters::DepthTest::IfLessOrEqual,
            write: true,
            range: (0.0, 1.0),
            clamp: glium::draw_parameters::DepthClamp::NoClamp,
        };

        for (sid, batch) in self.batches.iter().enumerate() {
            let sampler = glium::uniforms::Sampler::new(&self.batches[sid].texture)
                .magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
                .minify_filter(glium::uniforms::MinifySamplerFilter::Nearest);
            let uniforms = uniform! {
                u_vp_size: dim,
                u_sampler: sampler,
            };

            let bkt = &batch.bucket;
            let quads = bkt.len() / 4;
            if quads > 0 {
                // TODO: first fill vb with everything, then do all calls
                // since write will block until prev draw is finished
                let slice = self.vb.slice(0..quads * 4).unwrap();
                slice.write(bkt);

                let ids = self.ib.slice(0..quads * 6).unwrap();

                if let Err(e) = frame.draw(&self.vb, &ids, &self.program, &uniforms, &params) {
                    println!("Draw call error: {:?}", e);
                }
            }
        }
    }
}
