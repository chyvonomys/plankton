enum Door {
    Entrance,
    Stairs(u8, u8), // where do they lead to
    Kitchen(u8), // id
    PlayRoom,
    LoungeRoom,
    MeetingRoom(u8), // id
    DevRoom(u8), // id
    RestRoom,
    Wall,
}

struct Floor {
    elements: Vec<Door>,
}

struct Level {
    floors: Vec<Floor>,
}

enum Role {
    Player,
    Director,
    Accountant,
    Stalin,
    Podran,
}

struct Character {
    floor: u8,
    position: f32, // on a floor
    east: bool, // turned right - true, left - false
    door: Option<Door>,
    role: Role,
}

struct Game {
    level: Level,
    player: Character,
    enemies: Vec<Character>,
}

/*

SAMPLE LEVEL:


  S w w D w M w w K w R w w E w w D w D w w S w P 
  S w D w w D w K w w R w w w w D w M w D w S w P


 */
