use math::{self, Vec2};
use math::consts::*;
use screen::{Entities, Entity, TimeInfo, PositionItem};
use collision::{CollisionItem, CollisionResponse, MovementItem};
use sprite::SpriteItem;

#[derive(Serialize, Deserialize, Clone, Copy, Debug)]
enum Direction { DirL, DirR }

#[derive(Serialize, Deserialize, Clone, Copy, Debug)]
enum SashaMode { Idle, Walk, Jump, Fall }

#[derive(Serialize, Deserialize, Clone, Copy, Debug)]
enum OrbMode { Idle, Fall, Carried }

#[derive(Serialize, Deserialize, Clone, Copy, Debug)]
pub enum JumpInput { J0, J1 }

#[derive(Serialize, Deserialize, Clone, Copy, Debug)]
pub enum LeftInput { L0, L1 }

#[derive(Serialize, Deserialize, Clone, Copy, Debug)]
pub enum RightInput { R0, R1 }

#[derive(Serialize, Deserialize, Clone, Copy, Debug)]
pub enum CarryInput { C0, C1 }

#[derive(Serialize, Deserialize, Debug)]
pub struct SashaInput(pub LeftInput, pub RightInput, pub JumpInput);

#[derive(Serialize, Deserialize, Debug)]
pub struct OrbInput(pub CarryInput);

#[derive(Serialize, Deserialize, Debug)]
struct SashaState(Direction, SashaMode, f32);

#[derive(Serialize, Deserialize, Debug)]
struct OrbState(OrbMode, f32);

#[derive(Debug, Serialize, Deserialize)]
pub struct SashaItem {
    pub input: SashaInput,
    state: SashaState,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct OrbItem {
    pub input: OrbInput,
    state: OrbState,
}

#[derive(Debug, Serialize, Deserialize)]
pub enum AnimationItem {
    Sasha(SashaItem),
    Orb(OrbItem),
}

#[derive(Debug)]
enum BlockInput { Free, Side, Ceiling, Floor }

impl AnimationItem {
    pub fn sasha() -> Self {
        AnimationItem::Sasha(SashaItem {
            input: SashaInput(LeftInput::L0, RightInput::R0, JumpInput::J0),
            state: SashaState(Direction::DirR, SashaMode::Walk, ZERO),
        })
    }

    pub fn orb() -> Self {
        AnimationItem::Orb(OrbItem {
            input: OrbInput(CarryInput::C0),
            state: OrbState(OrbMode::Fall, ZERO),
        })
    }
}

const FRAMES_PER_SEC: f32 = 20.0f32;

// NOTE: this should be in sync with animation spritesheet.
const SASHA_FRAMES: usize = 12;
const SASHA_DELTAS: [f32; SASHA_FRAMES] =
    [8.0, 7.0, 7.0, 6.0, 9.0, 8.0, 8.0, 9.0, 7.0, 9.0, 7.0, 10.0];

fn walk_dx(t0: f32, t1: f32) -> f32 {
    let f0 = t0 * FRAMES_PER_SEC;
    let f1 = t1 * FRAMES_PER_SEC;
    let f0i = f0.trunc() as usize;
    let f1i = f1.trunc() as usize;

    if f0i == f1i {
        (f1 - f0) * SASHA_DELTAS[f0i % SASHA_FRAMES]
    } else {
        let f0sub = ONE - f0.fract();
        let f1sup = f1.fract();

        let mut res =
            f0sub * SASHA_DELTAS[f0i % SASHA_FRAMES] + f1sup * SASHA_DELTAS[f1i % SASHA_FRAMES];

        for f in f0i + 1..f1i {
            res += SASHA_DELTAS[f % SASHA_FRAMES];
        }

        res
    }
}

fn free_dx(t0: f32, t1: f32) -> f32 {
    120f32 * (t1 - t0)
}

const JUMP_DUR: f32 = 0.5f32;
const JUMP_HEIGHT: f32 = 200f32;

#[inline]
fn jump_curve(t: f32) -> f32 {
    JUMP_HEIGHT * (t / JUMP_DUR * HALFPI).sin()
}

fn jump_dy(t0: f32, t1: f32) -> f32 {
    jump_curve(t1) - jump_curve(t0)
}

const FALL_DUR: f32 = 0.4f32;
const FALL_HEIGHT: f32 = 200f32;

const FALL_SPEED_SUS: f32 = -FALL_HEIGHT / FALL_DUR * HALFPI;

#[inline]
fn fall_curve(t: f32) -> f32 {
    FALL_HEIGHT * (t / FALL_DUR * HALFPI).cos()
}

fn fall_dy(t0: f32, t1: f32) -> f32 {
    if t0 > FALL_DUR {
        FALL_SPEED_SUS * (t1 - t0)
    } else {
        fall_curve(t1) - fall_curve(t0)
    }
}

// TODO: tests!

fn calc_next_sasha(st: &SashaState, inp: &SashaInput, block: BlockInput, dt: f32) -> (SashaState, f32, f32) {
    use self::Direction::*;
    use self::SashaMode::*;
    use self::LeftInput::*;
    use self::RightInput::*;
    use self::JumpInput::*;
    use self::BlockInput::*;

    let grdy = FOUR * FALL_SPEED_SUS * dt;
    let separate_dist = 0.015f32;

    let (d, s, tn, dx, dy) = match (st.0, st.1, st.2, inp.0, inp.1, inp.2, block) {
        // Touch
        (dir,  Walk, _, _,  _,  _,  Side) |
        (dir,  Idle, _, _,  _,  _,  Side) => (dir, Idle, ZERO, ZERO, grdy),
        // Hit ceiling
        (dir,  _,    _, _,  _,  _,  Ceiling) => (dir, Fall, ZERO, ZERO, -separate_dist),
        // Invalid
        (dir,  Walk, _, _,  _,  _,  Free) => (dir, Fall, ZERO, ZERO, ZERO),
        (dir,  Idle, _, _,  _,  _,  Free) => (dir, Fall, ZERO, ZERO, ZERO),
        // Continue jump (Free/Side/Floor)
        (dir,  Jump, t, L1, R1, _,  _) |
        (dir,  Jump, t, L0, R0, _,  _) if t < JUMP_DUR => (dir,  Jump, t+dt,  ZERO,             jump_dy(t, t+dt)),
        (_,    Jump, t, L1, R0, _,  _) if t < JUMP_DUR => (DirL, Jump, t+dt, -free_dx(t, t+dt), jump_dy(t, t+dt)),
        (_,    Jump, t, L0, R1, _,  _) if t < JUMP_DUR => (DirR, Jump, t+dt,  free_dx(t, t+dt), jump_dy(t, t+dt)),
        // Release jump
        (dir,  Jump, _, _,  _,  _,  _) => (dir, Fall, ZERO, ZERO, ZERO),
        // Fall on ground
        (dir,  Fall, _, _,  _,  _,  Floor) => (dir, Idle, ZERO, ZERO, grdy),
        // Falling (free/side)
        (dir,  Fall, t, L0, R0, _,  _) |
        (dir,  Fall, t, L1, R1, _,  _) => (dir,  Fall, t+dt,  ZERO,             fall_dy(t, t+dt)),
        (_,    Fall, t, L1, R0, _,  _) => (DirL, Fall, t+dt, -free_dx(t, t+dt), fall_dy(t, t+dt)),
        (_,    Fall, t, L0, R1, _,  _) => (DirR, Fall, t+dt,  free_dx(t, t+dt), fall_dy(t, t+dt)),
        // (everything below is Floor)
        // Idling
        (dir,  Idle, t, L0, R0, J0, _) |
        (dir,  Idle, t, L1, R1, J0, _) => (dir, Idle, t+dt, ZERO, grdy),
        // Init jump
        (dir,  Idle, _, _,  _,  J1, _) |
        (dir,  Walk, _, _,  _,  J1, _) => (dir, Jump, dt, ZERO, jump_dy(ZERO, dt)),
        // Stop moving
        (dir,  Walk, _, L0, R0, J0, _) |
        (dir,  Walk, _, L1, R1, J0, _) => (dir, Idle, ZERO, ZERO, grdy),
        // Start moving
        (_,    Idle, _, L1, R0, J0, _) => (DirL, Walk, dt, -walk_dx(ZERO, dt), grdy),
        (_,    Idle, _, L0, R1, J0, _) => (DirR, Walk, dt,  walk_dx(ZERO, dt), grdy),
        // Continue moving
        (DirL, Walk, t, L1, R0, J0, _) => (DirL, Walk, t+dt, -walk_dx(t, t+dt), grdy),
        (DirR, Walk, t, L0, R1, J0, _) => (DirR, Walk, t+dt,  walk_dx(t, t+dt), grdy),
        // Turn opposite
        (DirL, Walk, _, L0, R1, J0, _) => (DirR, Walk, ZERO, ZERO, grdy),
        (DirR, Walk, _, L1, R0, J0, _) => (DirL, Walk, ZERO, ZERO, grdy),
    };

    (SashaState(d, s, tn), dx, dy)
}

fn calc_next_orb(st: &OrbState, inp: &OrbInput, block: BlockInput, dt: f32) -> (OrbState, f32, f32) {
    use self::OrbMode::*;
    use self::CarryInput::*;
    use self::BlockInput::*;

    let grdy = FOUR * FALL_SPEED_SUS * dt;

    let (s, tn, dx, dy) = match (st.0, st.1, inp.0, block) {
        // Hit the floor
        (Fall,    _, C0, Floor) => (Idle, ZERO, ZERO, ZERO),
        // Continue falling
        (Fall,    t, C0,     _) => (Fall, t+dt, ZERO, fall_dy(t, t+dt)),
        // Continue standing
        (Idle,    t, C0, Floor) => (Idle, t+dt, ZERO, grdy),
        // Start falling
        (Idle,    _, C0,     _) => (Fall, ZERO, ZERO, ZERO),
        // Continue carry    
        (Carried, t, C1,     _) => (Carried, t+dt, ZERO, ZERO),
        // Pick up           
        (_,       _, C1,     _) => (Carried, ZERO, ZERO, ZERO),
        // Drop              
        (Carried, _, C0,     _) => (Fall, ZERO, ZERO, ZERO),
    };
    (OrbState(s, tn), dx, dy)
}

pub struct AnimationSystem {}

impl AnimationSystem {
    pub fn new() -> Self {
        Self {}
    }

    fn process(dt: f32, aitem: &mut AnimationItem,
               citem: &Option<CollisionItem>, mitem: &mut Option<MovementItem>,
               pitem: &Option<PositionItem>, sitem: &mut Option<SpriteItem>) {

        let block = match *citem {
            Some(ref c) => match *c.get_response() {
                CollisionResponse::StandsOn(_) => BlockInput::Floor,
                CollisionResponse::Supports(_) => BlockInput::Ceiling,
                CollisionResponse::PushesLeft(_) |
                CollisionResponse::PushesRight(_) |
                CollisionResponse::StandsOnAndPushesLeft(_, _) |
                CollisionResponse::StandsOnAndPushesRight(_, _)=> BlockInput::Side,
                _ => BlockInput::Free,
            },
            _ => BlockInput::Free,
        };

        let (dx, dy) = match aitem {
            &mut AnimationItem::Sasha(SashaItem { ref input, ref mut state }) => {
                let (new_state, dx, dy) = calc_next_sasha(state, input, block, dt);

                let frame_idx = match new_state.1 {
                    SashaMode::Walk => (new_state.2 * FRAMES_PER_SEC) as u32,
                    SashaMode::Idle | SashaMode::Fall => 0,
                    SashaMode::Jump => 3,
                };

                let mirror = match new_state.0 {
                    Direction::DirL => true,
                    Direction::DirR => false,
                };

                *sitem = Some(::sprite::SpriteItem::sasha(frame_idx, mirror));
                *state = new_state;
                (dx, dy)
            },
            &mut AnimationItem::Orb(OrbItem { ref input, ref mut state }) => {
                let (new_state, dx, dy) = calc_next_orb(state, input, block, dt);
                *state = new_state;
                (dx, dy)
            },
        };

        if let (&mut Some(ref mut m), &Some(ref p)) = (mitem, pitem) {
            let dp = Vec2::new(dx, dy);
            m.planned_position = math::add(&p.origin, &dp);
        }
    }

    pub fn turn(&mut self, tinfo: &TimeInfo, es: &mut Entities) {

        let dt = tinfo.get_delta_seconds();

        // We work with active items that have animation enabled
        // everything else is not required
        for (_, e) in es.all_mut() {
            if let Entity {
                active: true,
                animation: Some(ref mut aitem),
                ref collision,
                ref mut movement,
                ref position,
                ref mut sprite,
                ..
            } = *e {
                Self::process(dt, aitem, collision, movement, position, sprite);
            }
        }
    }
}
