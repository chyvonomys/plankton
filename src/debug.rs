use glium;
use glium::backend::Facade;
use glium::Surface;
use screen::{Entities, Entity, TimeInfo};
use input::InputInfo;
use math::{self, Vec2};
use math::consts::*;

#[derive(Copy, Clone)]
struct Vtx2P4C {
    i_ss_position: [f32; 2],
    i_color: [f32; 4],
}

implement_vertex!(Vtx2P4C, i_ss_position, i_color);

pub struct DebugSystem {
    vs: Vec<Vtx2P4C>,
    program: glium::Program,
    vb: glium::VertexBuffer<Vtx2P4C>,
    ib: glium::IndexBuffer<u16>,
}

impl DebugSystem {
    pub fn new(display: &glium::Display) -> Self {
        let vert_src = include_str!("shaders/basic-color-v.glsl");
        let frag_src = include_str!("shaders/basic-color-f.glsl");

        let facade = display.get_context();

        let program = glium::Program::new(
            facade,
            glium::program::ProgramCreationInput::SourceCode {
                vertex_shader: vert_src,
                tessellation_control_shader: None,
                tessellation_evaluation_shader: None,
                geometry_shader: None,
                fragment_shader: frag_src,
                transform_feedback_varyings: None,
                outputs_srgb: true, // This tells glium not to do gamma correction.
                uses_point_size: false,
            },
        ).unwrap();

        const NUM_LINES: usize = 1000;

        let line_indexes: Vec<u16> = (0..NUM_LINES as u16 * 2).collect();

        let vb = glium::VertexBuffer::empty_persistent(facade, NUM_LINES * 2).unwrap();
        let ib = glium::index::IndexBuffer::immutable(
            facade,
            glium::index::PrimitiveType::LinesList,
            &line_indexes,
        ).unwrap();

        let vs = Vec::new();

        Self { vs, program, vb, ib }
    }

    #[inline]
    fn add_segment(&mut self, p0: [f32; 2], p1: [f32; 2], color: [f32; 4]) {
        self.vs.push(Vtx2P4C { i_ss_position: p0, i_color: color });
        self.vs.push(Vtx2P4C { i_ss_position: p1, i_color: color });
    }

    fn add_rectangle(&mut self, bl: &Vec2, tr: &Vec2) {
        let color = [ZERO, ONE, ONE, ONE];

        let b = bl.y;
        let l = bl.x;
        let t = tr.y;
        let r = tr.x;

        self.add_segment([l, b], [r, b], color);
        self.add_segment([r, b], [r, t], color);
        self.add_segment([r, t], [l, t], color);
        self.add_segment([l, t], [l, b], color);
    }

    fn add_arc(&mut self, o: &Vec2, r: &Vec2, angle: f32, color: &[f32; 4]) {

        let step = angle / 32f32;

        let mut p0 = math::add(r, o);
        let mut a = ZERO;

        for _ in 0..32 {
            a += step;

            let p1 = math::add(&math::rot(r, a), o);
            
            self.add_segment(p0.arr(), p1.arr(), *color);

            p0 = p1;
        }
    }

    /*
    fn add_circle(&mut self, r: f32, o: &Vec2) {
        self.add_arc(o, &Vec2::new(r, ZERO), TWOPI, &[ZERO, ONE, ONE, ONE]);
    }
    */

    fn add_cross(&mut self, r: f32, o: &Vec2) {
        let color = [ZERO, ONE, ONE, ONE];

        self.add_segment([o.x - r, o.y], [o.x + r, o.y], color);
        self.add_segment([o.x, o.y - r], [o.x, o.y + r], color);
    }

    // None if p inside, (left, right) if outside (from p's point of view)
    fn find_touch_pts(o: &Vec2, r: f32, p: &Vec2) -> Option<(Vec2, Vec2)> {
        let op = math::sub(p, o);
        let op_len = math::len(&op);

        if op_len < r {
            None
        } else {
            let cos = r / op_len;
            let angle = cos.acos();

            let opr = math::mul(r, &math::norm(&op));

            Some((
                math::add(&o, &math::rot(&opr, -angle)),
                math::add(&o, &math::rot(&opr, angle)),
            ))
        }
    }

    fn add_path(&mut self, src_pos: &Vec2, src_dir: &Vec2, dst_pos: &Vec2) {
        let dir_color = [ZERO, ZERO, ONE, ONE]; // blue
        let diff_color = [HALF, HALF, HALF, ONE]; // gray
        let path_color = [ONE, HALF, ZERO, ONE]; // orange
        let turn_radius = 30f32;
        let dir_length = 50f32;

        let dir = math::norm(src_dir);
        let dir_pos = math::add(&src_pos, &math::mul(dir_length, &dir));

        self.add_segment(src_pos.arr(), dir_pos.arr(), dir_color);
        self.add_segment(src_pos.arr(), dst_pos.arr(), diff_color);

        let ccw = math::cross(&dir, &math::sub(dst_pos, src_pos)) > ZERO;

        let to_center = math::mul(if ccw { ONE } else { -ONE }, &math::perp(&dir));

        let center = math::add(&src_pos, &math::mul(turn_radius, &to_center));

        if let Some((lp, rp)) = Self::find_touch_pts(&center, turn_radius, &dst_pos) {
            //self.add_circle(turn_radius, &center);
            let touch = if ccw { lp } else { rp };
            let r0 = math::mul(turn_radius, &math::neg(&to_center));
            let r1 = math::sub(&touch, &center);
            if ccw {
                let mut angle = math::ccw_angle(&r0, &r1);
                if angle < ZERO { angle += TWOPI };
                self.add_arc(&center, &r0, angle, &path_color);
            } else {
                let mut angle = math::ccw_angle(&r1, &r0);
                if angle < ZERO { angle += TWOPI };
                self.add_arc(&center, &r0, -angle, &path_color);
            }
            self.add_segment(touch.arr(), dst_pos.arr(), path_color);
        } else {
            self.add_segment(src_pos.arr(), dst_pos.arr(), path_color);
        }
    }

    pub fn turn(&mut self, tinfo: &TimeInfo, es: &Entities, frame: &mut glium::Frame, iinfo: &InputInfo) {
        self.vs.clear();

        let mut under_mouse = None;
        let m = iinfo.mouse_pos.clone();

        // 1. collidable entities
        for (eid, e) in es.all() {
            if let Entity {
                active: true,
                collision: Some(::collision::CollisionItem { ref shape, .. }),
                position: Some(::screen::PositionItem { ref origin, .. }),
                ..
            } = *e {
                match *shape {
                    ::collision::CollisionShape::Rect(w, h) => {
                        let s = Vec2::new(w, h);
                        self.add_rectangle(
                            &math::sub(origin, &s),
                            &math::add(origin, &s),
                        );
                        self.add_cross(w.min(h) * 0.5f32, origin);

                        if (m.x - origin.x).abs() < w && (m.y - origin.y).abs() < h {
                            under_mouse = Some(eid);
                        }
                    },
                    /*
                    ::collision::CollisionShape::Disk(r) => {
                        self.add_circle(r, origin);
                        self.add_cross(r * 0.5f32, origin);
                    }
                    */
                }
            }
        }

        if let Some(true) = iinfo.down.last().and_then(|la| Some(la.0 == ::input::Action::Press && tinfo.is_recent(la.1))) {
            println!("UNDER MOUSE: {:?} -> {:#?}", under_mouse, under_mouse.and_then(|eid| es.get(eid)));
        }

        for (_, e) in es.all() {
            if let Entity {
                active: true,
                path: Some(::screen::PathItem { ref points }),
                ..
            } = *e {
                for i in 1..points.len() {
                    self.add_segment(points[i-1].arr(), points[i].arr(), [ONE, 0.8f32, 0.7f32, ONE]);
                }
            }
        }

        self.add_path(&Vec2::new(400f32, 250f32), &Vec2::new(0f32, -1f32), &m);

        let mut params = glium::DrawParameters::default();
        params.depth = glium::Depth {
            test: glium::draw_parameters::DepthTest::Overwrite,
            write: false,
            range: (0.0, 1.0),
            clamp: glium::draw_parameters::DepthClamp::NoClamp,
        };
        let uniforms = uniform! { u_vp_size: [800f32, 500f32] };

        let lines = self.vs.len() / 2;
        if lines > 0 {
            let subview = self.vb.slice(0..lines * 2).unwrap();
            subview.write(&self.vs);

            let ids = self.ib.slice(0..lines * 2).unwrap();

            if let Err(e) = frame.draw(&self.vb, &ids, &self.program, &uniforms, &params) {
                println!("Draw call error: {:?}", e);
            }
        }

    }
}
